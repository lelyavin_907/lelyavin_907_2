

# INF 20.02

ru.kpfu.itis.group907.<фамилия>.inf.exceptions 

Exceptions

- 1 - 1/1
- 2 - 1/1
- 3 - 1/1
- 4 - 1/1
- 5 - 1/1
- 6 - 1/1

# ALG 21.02 

ru.kpfu.itis.group907.<фамилия>.algorithms.pair1

LinkedList + Sorting + Accountable

- LinkedList - 8/9
- merge - 0/2
- diff - 0/2
- intersection - 0/2
- generic Accountable - 0/5

# INF 27.02 

ru.kpfu.itis.group907.<фамилия>.inf.exceptions

Exceptions 2

- IDNumber - 5/5
- Point - 5/5
- Convert - 5/5
- SumException - 5/5
- RunnerProcessor - 0/5
- ThrowsExceptions - 5/5

# ALG 28.02 

ru.kpfu.itis.group907.<фамилия>.algorithms.pair2

Sorting

- merge - 1/2
- largest number - 2/2

# INF 02.03

ru.kpfu.itis.group907.<фамилия>.inf.generics

Generics

- LinkedList - 5/5
- Array - 0/5
- test on Account<String, Integer> - 0/5

# INF 06.03 

ru.kpfu.itis.group907.<фамилия>.inf.comparable

Comparable

- 1 - 5/5
- 2 - 5/5
- 3 - 5/5
- 4 - 5/5


# INF 10.03 

ru.kpfu.itis.group907.<фамилия>.inf.maps

Maps

- 1 - 5/5
- 2 - 5/5

# ALG 13.03 

ru.kpfu.itis.group907.<фамилия>.algorithms.pair3

Sets

- 1 - 2/2
- 2 - 2/2

# INF KR1 16.03 10:20

ru.kpfu.itis.group907.<фамилия>.inf.kr1

9/10


# INF 26.03

ru.kpfu.itis.group907.<фамилия>.inf.iterator

Iterator

- 1 - 0/5
- 2 - 5/5
- 3 - 0/5

# ALG 27.03

ru.kpfu.itis.group907.<фамилия>.algorithms.pair4

- 1 - 2/2
- 2 - 0/2
- 3 - 0/2

# INF 31.03

ru.kpfu.itis.group907.<фамилия>.inf.tree

Trees

- 1 - 0/5
- 2 - 0/5
- 3 - 0/5
- 4 - 0/5

# ALG 03.04

ru.kpfu.itis.group907.<фамилия>.algorithms.pair5

Dynamics

- 1 - 0/2
- 2 - 0/2
- 3 - 0/2


# ALG SEM1 03.04



# INF Stack
 
ru.kpfu.itis.group907.<фамилия>.inf.stack

* это даты пар, на которых должно было выполняться задание
- Stack (13.04) - 8/10
- RPN (15.04) - 8/10
- RPN reformat (16.04) - 7/10 
- combine all (20.04) - 0/10
- sorting (23.04) - 8/10
 
 
# ALG 24.04
 
ru.kpfu.itis.group907.<фамилия>.algorithms.pair7
 
- 1 - 2/2
- 2 - 1/2

Жду исправлений




# ALG 01.05

ru.kpfu.itis.group907.<фамилия>.algorithms.pair8

- 1 - 2
- 2 - 2
- 3 - 2
- 4 - 0


# INF 06.05

ru.kpfu.itis.group907.<фамилия>.inf.stack

- reformat - 10


# INF 10.05

ru.kpfu.itis.group907.<фамилия>.inf.strings

- 1 - 5

Можно было в принципе просто всю строку к нижнему регистру приветсти и проверять элемент на букву, работало бы быстрее
- 2 - 5
- 3 - 5
- 4 - 4


# INF 12.05

ru.kpfu.itis.group907.<фамилия>.inf.regex

- 1 - 5
- 2 - 5
- 3 - 5


# ALG 15.05

ru.kpfu.itis.group907.<фамилия>.algorithms.pair9

- 1 - 2
- 2 - 2
- 3 - 2
- 4 - 0


# INF 18.05

ru.kpfu.itis.group907.<фамилия>.inf.files

- java files - 5

Говорят в мапах можно хранить ключ-значение)))
- url - 0


# ALG 15.05

ru.kpfu.itis.group907.<фамилия>.algorithms.pair10

- 1 - 0
- 2 - 0
- 3 - 0
- 4 - 0



# INF 25.05 10:12



- Fuel - [x] 
- Oxidizer - [x]
- Kerosene и Hydrogen - [x]
- LiquidOxygen - [x]
- InsufficientMatterException - [-]
- Tank<T> - [-]
- UnstableEngineException - [x]
- RocketComparator - [x]
- Code style - [x]
- It works - [-]

Total: 7

