package ru.kpfu.itis.group907.lelyavin.inf.kr1;

import java.util.LinkedList;

public class Queue<T> {
    private LinkedList<T> linkedList = new LinkedList<>();

    public void add(T element) {
        linkedList.add(element);
    }

    public T poll() {
        try {
            T t = linkedList.getLast();
            linkedList.pollLast();
            return t;

        } catch (QueulsEmptyException e) {
            System.out.println(e);
        }
        return null;
    }

    public String toString() {
        String string = "";
        Object[] objects = linkedList.toArray();
        for (int i = 0; i < objects.length; i++) {
            string += objects[i] + " ";
        }
        return string;
    }

    public Object[] toArray() {
        Object[] objects = new Object[linkedList.size()];
        for (int i = 0; i < linkedList.size(); i++) {
            objects[i] = linkedList.get(i);
        }
        return objects;
    }

    public boolean isEmpty() {
        Object[] objects = linkedList.toArray();
        if (objects.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    public int Size() {
        Object[] objects = linkedList.toArray();
        return objects.length;
    }

    public void Clear() {
        linkedList.clear();
    }
}
