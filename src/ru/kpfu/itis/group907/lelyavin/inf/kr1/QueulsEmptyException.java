package ru.kpfu.itis.group907.lelyavin.inf.kr1;

import java.util.NoSuchElementException;

public class QueulsEmptyException extends NoSuchElementException {
    public String toString() {
        return ("Этот элемент отсутствует");
    }
}
