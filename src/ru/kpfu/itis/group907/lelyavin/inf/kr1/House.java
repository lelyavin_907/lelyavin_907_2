package ru.kpfu.itis.group907.lelyavin.inf.kr1;

public class House implements Comparable<House> {
    int length;
    int width;

    House(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int Area() {
        return this.width * this.length;
    }

    @Override
    public int compareTo(House house) {
        if (this.width * this.length == house.length * house.width) {
            return 0;
        }
        return (this.width * this.length > house.length * house.width) ? 1 : -1;
    }
}
