package ru.kpfu.itis.group907.lelyavin.inf.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task1 {
    public static void main(String[] args) {
        String dateFirst = "03/06/1237 12:00";
        String dateIntermediate = "11/12/1754 21:35";
        String dateLast = "02/27/1978 21:35";

        Pattern pattern = patternCompile();

        Matcher matcherFirst = pattern.matcher(dateFirst);
        Matcher matcherIntermediate = pattern.matcher(dateIntermediate);
        Matcher matcherLast = pattern.matcher(dateLast);

        if (matcherFirst.matches() && matcherIntermediate.matches() && matcherLast.matches()) {
            System.out.println(dateFirst + "\n" + dateIntermediate +"\n" + dateLast);
        }
    }

    public static Pattern patternCompile(){//mm/dd/yyyy hh/mm Кажется можно было короче и я не учел некоторые мелочи TODO учесть некоторые мелочи
        return Pattern.compile("((02/07/1237) ([1][2-9]|2[0-3]):([0-5]\\d))|" +
                "(((02)/(0[8-9]|[1-2]\\d|3[0-1])/1237) ([0-1]\\d|2[0-3]):([0-5]\\d))|" +
                "(((0[3-9]|1[0-2])/(0[1-9]|[1-2]\\d|3[0-1])/1237) ([0-1]\\d|2[0-3]):([0-5]\\d))|" +
                "(((0[1-9]|1[0-2])/(0[1-9]|[1-2]\\d|3[0-1])/(123[7-9]|12[4-9]\\d|1[3-8]\\d\\d)) ([0-1]\\d|2[0-3]):([0-5]\\d))|" +
                "(((0[1-9]|1[0-2])/(0[1-9]|[1-2]\\d|3[0-1])/(19[0-6]\\d|197[0-7])) ([0-1]\\d|2[0-3]):([0-5]\\d))|" +
                "((01/(0[1-9]|[1-2]\\d|3[0-1]))|(02/(0[1-9]|1\\d|2[0-6])/1978) ([0-1]\\d|2[0-3]):([0-5]\\d))|" +
                "((02/27/1978) ([0-1]\\d:[0-5]\\d|20:[0-5]\\d|21:[0-2]\\d|21:3[0-5]))");
    }
}
