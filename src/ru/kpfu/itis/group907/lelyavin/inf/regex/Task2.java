package ru.kpfu.itis.group907.lelyavin.inf.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2 {
    public static void main(String[] args) {
        String string1 = "0";
        String string2 = "+2";

        Pattern pattern = patternCompile();
        Matcher matcherFirst = pattern.matcher(string1);

        if (matcherFirst.matches()) {
            System.out.println(string1);
        }

    }
    public static Pattern patternCompile() {
        return Pattern.compile("([-+]?[1-9]+([.,]\\d*(\\(\\d*\\))?)?)|" + "0|" + "[-+]?0+([.,]+\\d*(\\(\\d*\\))?){1}");
    }
}
