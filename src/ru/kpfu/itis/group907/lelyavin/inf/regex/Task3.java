package ru.kpfu.itis.group907.lelyavin.inf.regex;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task3 {
    public static void main(String[] args) {//113432522212221113248
        Random random = new Random();
        Pattern pattern = Pattern.compile("([^24680][^24680][^24680]|" +
                "[^24680][24680][^24680]|" +
                "[^24680][^24680][24680]|" +
                "[^24680][24680][24680]|" +
                "[24680][24680][^24680]" +
                "|[24680][^24680][^24680]" +
                "|[24680][^24680][24680])+");
        int j = 0;
        for (int i = 0; i < 10;) {
            String x = random.nextInt(Integer.MAX_VALUE) + "";
            Matcher matcher = pattern.matcher(x);
            j++;
            if(!matcher.matches()){
                i++;
                System.out.println(x);
            }
        }
        System.out.println(j);

    }
}
