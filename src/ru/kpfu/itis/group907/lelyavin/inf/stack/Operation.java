package ru.kpfu.itis.group907.lelyavin.inf.stack;

import java.io.*;
import java.util.NoSuchElementException;

public abstract class Operation implements StackElement {
    private int priority;
    private String operation;
    private int value;

    public Operation() {}

    public static Operation getOperation(String operation) {
        switch (operation) {
            case "+":
                return new SumOperation();
            case "-":
                return new SubOperation();
            case "*":
                return new MultOperation();
            case "/":
                return new DivOperation();
            default:
                return null;
        }
    }

    public int getPriority() {
        return this.priority;
    }



    public abstract int value(int int1, int int2);

    ;

    public abstract boolean isPushNeeded(Operation element);
}
