package ru.kpfu.itis.group907.lelyavin.inf.stack;

public class Flat {
    private int nFlat;
    private int length;
    private int width;

    Flat(int nFlat, int length, int width){
        this.nFlat = nFlat;
        this.length = length;
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "nFlat=" + nFlat +
                ", length=" + length +
                ", width=" + width +
                '}';
    }
}
