package ru.kpfu.itis.group907.lelyavin.inf.stack;

public class MultOperation extends Operation{
    private int priority = 2;
    private String operation;

    public MultOperation() {
        this.operation = "*";
    }

    @Override
    public int value(int int1, int int2) {
        return int1 * int2;
    }

    public String getOperation() {
        return operation;
    }

    @Override
    public int value() {
        return 0;
    }

    public boolean isPushNeeded(Operation element) {
        return this.getPriority() <= element.getPriority();
    }
}
