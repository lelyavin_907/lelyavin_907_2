package ru.kpfu.itis.group907.lelyavin.inf.stack;

import java.util.*;

public class Stack<T> implements List<T> {
    List<T> linkedList;

    Stack() {
        this.linkedList = new LinkedList<>();
    }

    @Override
    public int size() {
        return  this.linkedList.size();
    }

    @Override
    public boolean isEmpty() {
        return this.linkedList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        Object[] objects =  this.linkedList.toArray();
//        for (int i = 0; i < this.linkedList.size(); i++) {
//            System.out.println(objects[i]);
//        }
        return objects;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }


    @Override
    public boolean add(Object e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public T get(int index) {
        return null;
    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    // можно такие вещи на одной строке оставлять
    @Override
    public void add(int index, Object element) {}

    @Override
    public T remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return this.linkedList.listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    public void push(T node) {
        this.linkedList.add(node);
    }

    public T pop() {
        T t = this.linkedList.get(this.linkedList.size() - 1);
        this.linkedList.remove(this.linkedList.size() - 1);
        return t;
    }

    public T peek() {
        T t = this.linkedList.get(0);
        this.linkedList.remove(0);
        return t;
    }

    public T getLast(){
        return this.linkedList.get(this.linkedList.size() - 1);
    }

    public String toString(){
        return this.linkedList.toString();
    }
}