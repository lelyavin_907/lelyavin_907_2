package ru.kpfu.itis.group907.lelyavin.inf.stack;

import java.io.*;
import java.util.NoSuchElementException;

public class Number implements StackElement {
    private int value;

    public Number(String value) {
        this.value = Integer.parseInt(value);
    }

    public String toString(){
        return this.value + "";
    }

//    public int parsInt(StackElement element){
//
//    }

    @Override
    public int value() {
        return this.value;
    }
}
