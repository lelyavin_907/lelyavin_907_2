package ru.kpfu.itis.group907.lelyavin.inf.stack;

public class DivOperation extends Operation{
    private int priority = 2;
    private String operation;

    public DivOperation() {
        this.operation = "/";
    }

    public String getOperation() {
        return operation;
    }

    @Override
    public int value(int int1, int int2) {
        return int2 / int1;
    }

    public boolean isPushNeeded(Operation element) {
        return this.getPriority() <= element.getPriority();
    }

    @Override
    public int value() {
        return 0;
    }
}
