package ru.kpfu.itis.group907.lelyavin.inf.stack;

import java.util.Comparator;

public class MyComparator implements Comparator<Flat> {

    // можно прям совсем в одну строку)
    @Override
    public int compare(Flat o1, Flat o2) {
        System.out.println(o1);
        if(o1.getLength() * o1.getWidth() == o2.getLength() * o2.getWidth()){
            return 0;
        }
        return o1.getLength() * o1.getWidth() > o2.getLength() * o2.getWidth() ? 1 : -1;
    }
}
