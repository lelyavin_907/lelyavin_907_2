package ru.kpfu.itis.group907.lelyavin.inf.stack;

public class Node<T> {
    public Node<T> next;
    public T elem;

    public Node(T elem) {
        this.elem = elem;
        this.next = null;
    }

    @Override
    public String toString() {
        return this.elem + " ";
    }
}
