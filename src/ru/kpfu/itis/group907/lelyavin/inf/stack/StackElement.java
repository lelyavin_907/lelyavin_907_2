package ru.kpfu.itis.group907.lelyavin.inf.stack;

public interface StackElement {

    public int value();
}
