package ru.kpfu.itis.group907.lelyavin.inf.stack;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class ReversePolishNotation {

    private Stack<Integer> intStack = new Stack<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ReversePolishNotation reversePolishNotation = new ReversePolishNotation();
        Stack<StackElement> polishNotation =  reform(scanner.nextLine());

        for (int i = 0; i < polishNotation.size(); i++) {
            reversePolishNotation.add(polishNotation.peek());
        }
        System.out.println(polishNotation);

    }


    public void add(int number) {
        this.intStack.push(number);
    }


    public void add(StackElement element) {
        if (element instanceof Operation) {
            ((Operation) element).value(this.intStack.pop(), this.intStack.pop());
        } else {
            try {
                this.add(element.value());
            } catch (NoSuchElementException | NumberFormatException e) {
                System.out.println("Вы ввели не правильное значение");
            }
        }
    }

//        switch (value){
//            case "+":
//                this.intStack.push(this.intStack.pop() + this.intStack.pop());
//                break;
//
//            case "-":
//                int saveElem = this.intStack.pop();
//                this.intStack.push( this.intStack.pop() - saveElem);
//                break;
//
//            case "*":
//                this.intStack.push(this.intStack.pop() * this.intStack.pop());
//                break;
//
//            case "/":
//                int saveElemOdd = this.intStack.pop();
//                this.intStack.push( this.intStack.pop() / saveElemOdd);
//                break;
//
//            default:
//                try{
//                    this.add(Integer.parseInt(value));
//                }
//                catch (NoSuchElementException | NumberFormatException e){
//                    System.out.println("Вы ввели не правильное значение");
//                }
//
//        }



    public static Stack<StackElement> reform(String equation){
        Stack<StackElement> result = new Stack<>();
        String[] equationArray = equation.split(" ");//5 + 5
        Stack<Operation> stringPolishNotation = new Stack<>();

        for (String s : equationArray) {
            Operation operation = Operation.getOperation(s);
            if (operation != null) {
                while (!stringPolishNotation.isEmpty() && operation.isPushNeeded(stringPolishNotation.getLast())) {
                    result.push(stringPolishNotation.pop());
                }
                stringPolishNotation.push(operation);
            } else {
//                try {
                result.push(new Number(s));
//                    res += Integer.parseInt(s) + " ";
//                } catch (NoSuchElementException | NumberFormatException e) {
//                    System.out.println("Вы ввели не правильное значение");
//                }
            }

//            switch (s) {
//
//                case "+":
//                case "-":
//                    if (stringPolishNotation.isEmpty()) {
//                        stringPolishNotation.push(s);
//                        break;
//                    }
//
//                    if (!stringPolishNotation.isEmpty()) {
//
//                        while (stringPolishNotation.getLast().equals("*") ||
//                                stringPolishNotation.getLast().equals("/") ||
//                                stringPolishNotation.getLast().equals("-") ||
//                                stringPolishNotation.getLast().equals("+")) {
//                            res += stringPolishNotation.pop() + " ";
//                            if (stringPolishNotation.isEmpty()) {
//                                break;
//                            }
//                        }
//                        stringPolishNotation.push(s);
//                        break;
//                    }
//                    stringPolishNotation.push(s);
//                    res += s + " ";
//                    break;
//
//
//                case "/":
//                case "*":
//                    if (stringPolishNotation.isEmpty()) {
//                        stringPolishNotation.push(s);
//                        break;
//                    } else if (stringPolishNotation.getLast().equals("/") || stringPolishNotation.getLast().equals("*")) {
//                        res += stringPolishNotation.pop() + " ";
//
//                        if (!stringPolishNotation.isEmpty()) {
//                            System.out.println(stringPolishNotation);
//                            while (stringPolishNotation.getLast().equals("*") ||
//                                    stringPolishNotation.getLast().equals("/")
//                            ) {
//                                res += stringPolishNotation.pop() + " ";
//                                if (stringPolishNotation.isEmpty()) {
//                                    break;
//                                }
//                            }
//                        }
//                        stringPolishNotation.push(s);
//                        break;
//                    }
//
//                    stringPolishNotation.push(s);
//                    break;
//
//                case "(":
//                    stringPolishNotation.push(s);
//                    break;
//
//                case ")":
//                    while (!stringPolishNotation.getLast().equals("(")) {
//                        try {
//                            res += stringPolishNotation.pop() + " ";
//                        } catch (NoSuchElementException e) {
//                            System.out.println("Вы ввели не парные скобки");
//                        }
//                    }
//                    stringPolishNotation.pop();
//                    break;
//
//                default:
//                    try {
//                        res += Integer.parseInt(s) + " ";
//                    } catch (NoSuchElementException | NumberFormatException e) {
//                        System.out.println("Вы ввели не правильное значение");
//                    }

//            }

        }
        //Цикл добавляет все знаки, которые остались в стеке
        for (int j = 0; j < stringPolishNotation.size(); j++) {
            result.push(stringPolishNotation.pop());
            j--;
        }
        return result;
    }
}
