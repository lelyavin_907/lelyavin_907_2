package ru.kpfu.itis.group907.lelyavin.inf.stack;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collections;
import java.util.Scanner;

public class Sorting {
    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\inf\\stack\\in.txt");
        Scanner scanner = new Scanner(fileReader);

        while(scanner.hasNext()){
            String[] strings = scanner.nextLine().split(" ");
            Stack<Flat> flatStack = new Stack<>();

            try{
                flatStack.push(new Flat(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]), Integer.parseInt(strings[2])));
            }
            catch (IndexOutOfBoundsException e){
                System.out.println("У квартиры не хватает данных для поиска площади.");
            }

            // можно было и без компаратора
            MyComparator myComparator = new MyComparator();
            Collections.sort(flatStack, myComparator);

            System.out.println(flatStack);
        }
    }
}
