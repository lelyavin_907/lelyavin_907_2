package ru.kpfu.itis.group907.lelyavin.inf.stack;

public class SubOperation extends Operation{
    private int priority = 1;
    private String operation;

    public SubOperation() {
        this.operation = "-";
    }

    @Override
    public int value(int int1, int int2) {
        return int2 - int1;
    }

    public String getOperation() {
        return operation;
    }

    @Override
    public int value() {
        return 0;
    }

    @Override
    public boolean isPushNeeded(Operation element) {
        return this.getPriority() <= element.getPriority();
    }
}
