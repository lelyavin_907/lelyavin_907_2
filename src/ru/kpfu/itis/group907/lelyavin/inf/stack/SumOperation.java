package ru.kpfu.itis.group907.lelyavin.inf.stack;

import java.io.*;

public class SumOperation extends Operation {
    private int priority = 1;
    private String operation;

    public SumOperation() {
        this.operation = "+";
    }

    @Override
    public int value(int int1, int int2) {
        return int1 + int2;
    }

    @Override
    public String toString() {
        return "+";
    }

    public String getOperation() {
        return operation;
    }

    @Override
    public int value() {
        return 0;
    }

    public boolean isPushNeeded(Operation element) {
        return this.getPriority() <= element.getPriority();
    }
}
