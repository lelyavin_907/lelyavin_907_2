package ru.kpfu.itis.group907.lelyavin.inf.maps.Task2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\inf\\maps\\Task2\\input.txt");
        Scanner scanner = new Scanner(fileReader);
        HashMap<String, Integer> hashMap = new HashMap<>();
        while (scanner.hasNext()) {
            String string = scanner.next().toLowerCase();
            if (hashMap.containsKey(string)) {
                int x = hashMap.get(string) + 1;
                hashMap.put(string, x);
            } else {
                hashMap.put(string, 1);
            }
        }
        System.out.println(hashMap);
    }
}
