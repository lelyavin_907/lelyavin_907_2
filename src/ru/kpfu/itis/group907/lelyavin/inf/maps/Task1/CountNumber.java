package ru.kpfu.itis.group907.lelyavin.inf.maps.Task1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CountNumber {
    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\inf\\maps\\input.txt");
        Scanner scanner = new Scanner(fileReader);
        Scanner scanner1 = new Scanner(fileReader);
        int i = 0;
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        while (scanner.hasNextInt()) {
            int local = scanner.nextInt();
            if (hashMap.containsKey(local)) {
                int x = hashMap.get(local) + 1;
                hashMap.put(local, x);
            } else {
                hashMap.put(local, 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            System.out.println(entry + " раз");
        }
    }
}
