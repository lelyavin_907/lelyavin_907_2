package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task2;

import java.util.Comparator;

public class MyComparator1 implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        int lengthO1 = o1;
        int lengthO2 = o2;
        if (o1 == 0 && o2 == 0) {
            return 0;
        } else if (o2 == 0) {
            return -1;
        } else if (o1 == 0) {
            return 1;
        }
        while (lengthO1 != 0 && lengthO2 != 0) {
            lengthO1 /= 10;
            lengthO2 /= 10;
        }
        if (lengthO1 == 0 && lengthO2 == 0) {
            return 0;
        }
        return (lengthO2 == 0) ? -1 : 1;
    }
}
