package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task3;

public class ConvenientStorage implements Comparable<ConvenientStorage> {
    String string;
    int[] ints;

    public String toString() {
        String s = "";
        for (int i = 0; i < this.ints.length; i++) {
            s += ints[i] + " ";
        }
        return s;
    }

    ConvenientStorage(String string, int[] ints) {
        this.string = string;
        this.ints = ints;
    }

    public String getString() {
        return this.string;
    }

    public int sum() {
        int sum = 0;
        for (int anInt : this.ints) {
            sum += anInt;
        }
        return sum;
    }

    @Override
    public int compareTo(ConvenientStorage o) {
        int i = 0;
        while ((i < this.string.length() && i < o.string.length())) {

            if (this.string.charAt(i) > o.string.charAt(i)) {
                return 1;
            } else if (this.string.charAt(i) < o.string.charAt(i)) {
                return -1;
            }
            i++;
        }
        if (this.string.length() == o.string.length()) {
            return 0;
        }
        return (o.string.length() < this.string.length()) ? 1 : -1;
    }

}
