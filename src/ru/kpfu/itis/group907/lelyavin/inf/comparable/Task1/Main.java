package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task1;

import ru.kpfu.itis.group907.lelyavin.inf.comparable.Task1.MyComparator;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList<String> arrayList = new ArrayList<String>();
        FileReader fileReader = new FileReader("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\inf\\comparable\\Task1\\in.txt");
        Scanner scanner = new Scanner(fileReader);
        String string = scanner.nextLine();
        String[] string1 = string.split(" ");
        File out = new File("out.txt");
        PrintWriter pwInput = new PrintWriter(out);
        int firstElem = 0;
        try {
            firstElem = Integer.parseInt(string1[0]);
            for (int i = 1; i < string1.length; i++) {
                if (string1[i].length() >= firstElem) {
                    arrayList.add(string1[i]);
                }
            }
            for (int i = 0; i < firstElem; i++) {
                MyComparator myComparator = new MyComparator(firstElem - 1);
                Collections.sort(arrayList, myComparator);
                pwInput.println(arrayList);
            }
        } catch (NumberFormatException e) {
            System.out.println("В вашем файле нет нужного значения");
        }
        fileReader.close();
        pwInput.close();
    }
}
