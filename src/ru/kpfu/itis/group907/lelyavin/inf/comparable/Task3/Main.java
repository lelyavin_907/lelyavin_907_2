package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        int[] ints = {1, 2, 3, 4, 5, 6, 7};
        String string = "abcbcbca.ru";
        String string2 = "a";
        ConvenientStorage convenientStorage1 = new ConvenientStorage(string, ints);
        ConvenientStorage convenientStorage2 = new ConvenientStorage(string2, ints);
        ArrayList<ConvenientStorage> arrayList = new ArrayList<>();
        arrayList.add(convenientStorage1);
        arrayList.add(convenientStorage2);

        File out = new File("out.txt");
        PrintWriter pwInput = new PrintWriter(out);
        for (int i = 0; i < arrayList.size(); i++) {
            pwInput.println(arrayList.get(i).string + " " + arrayList.get(i).sum());
        }
        pwInput.close();

        File out2 = new File("out2.txt");
        PrintWriter pwInput2 = new PrintWriter(out2);
        Collections.sort(arrayList);
        for (int i = 0; i < arrayList.size(); i++) {
            pwInput2.println(arrayList.get(i).string);
        }
        pwInput2.close();
    }
}
