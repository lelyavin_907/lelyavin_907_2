package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task4;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SortNaoborot {
    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\inf\\comparable\\Task4\\in.txt");
        Scanner scanner = new Scanner(fileReader);
        ArrayList<Integer> arrayList = new ArrayList<>();
        while (scanner.hasNextInt()) {
            arrayList.add(scanner.nextInt());
        }
        MyComparator myComparator = new MyComparator();
        Collections.sort(arrayList, myComparator);
        System.out.println(arrayList);
    }
}
