package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task1;

import java.util.Comparator;

class MyComparator implements Comparator<String> {
    private int k;

    MyComparator(int k) {
        super();
        this.k = k;
    }


    @Override
    public int compare(String o1, String o2) {
        if (o1.charAt(k) == o2.charAt(k)) {
            System.out.println(o1.charAt(k));
            return 0;
        }
        return (o1.charAt(k) < o2.charAt(k)) ? -1 : 1;
    }
}
