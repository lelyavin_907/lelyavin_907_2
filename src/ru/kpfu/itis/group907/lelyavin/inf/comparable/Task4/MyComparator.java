package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task4;

import java.util.Comparator;

public class MyComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        int iLocal = 0;
        int jLocal = 0;
        int o11 = o1;
        int o22 = o2;
        if ((o1 >= 0) && (o2 < 0)) {
            return 1;
        } else if ((o2 >= 0) && (o1 < 0)) {
            return -1;
        } else if ((o1 == 0) && (o2 == 0)) {
            return 0;
        }
        while (o11 != 0) {
            o11 /= 10;
            iLocal++;
        }
        while (o22 != 0) {
            o22 /= 10;
            jLocal++;
        }
        System.out.println(iLocal + " " + jLocal);
        if (iLocal > jLocal) {
            return 1;
        } else if (jLocal > iLocal) {
            return -1;
        }
        int[] intsO1 = new int[iLocal];
        int[] intsO2 = new int[jLocal];
        int x = 10;
        for (int i = 0; i < intsO1.length; i++) {
            if ((o2 % 10) > (o1 % 10)) {
                return -1;
            } else if ((o2 % 10) < (o1 % 10)) {
                return 1;
            }
            o2 /= 10;
            o1 /= 10;
        }
        return 0;
    }
}
