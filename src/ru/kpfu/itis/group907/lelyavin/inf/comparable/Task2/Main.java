package ru.kpfu.itis.group907.lelyavin.inf.comparable.Task2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\inf\\comparable\\Task2\\in.txt");
        Scanner scanner = new Scanner(fileReader);
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        String string = scanner.nextLine();
        String[] string1 = string.split(" ");
        try {
            for (String s : string1) {
                int x = Integer.parseInt(s);
                arrayList.add(x);
            }
            for (int i = 0; i < arrayList.size(); i++) {
                MyComparator1 myComparator1 = new MyComparator1();
                Collections.sort(arrayList, myComparator1);
            }
            System.out.println(arrayList);
        } catch (NumberFormatException e) {
            System.out.println("В вашем файле нет нужного значения");
        }
    }
}
