package ru.kpfu.itis.group907.lelyavin.inf.graphs;

import java.util.ArrayList;

public class MatrixAdjacency {
    private ArrayList<ArrayList<String>> arrayAdjacency;
    private String[] edgeArray;
    private String[] vertexArray;

    MatrixAdjacency(String[] edgeArray, String[] vertexArray){
        this.edgeArray = new String[edgeArray.length];
        System.arraycopy(edgeArray, 0, this.edgeArray, 0, edgeArray.length);
        this.vertexArray = new String[vertexArray.length];
        System.arraycopy(vertexArray, 0, this.vertexArray, 0, vertexArray.length);
        this.arrayAdjacency = new ArrayList<>();

        ArrayList<String> edgeArrayList = new ArrayList<>();
        for (int i = 0; i < this.edgeArray.length; i++) {
            edgeArrayList.add(this.edgeArray[i] + " ");
        }

        this.arrayAdjacency.add(edgeArrayList);
        for (int i = 0; i < this.vertexArray.length; i++)
        {
            ArrayList<String> incidenceArrayList = new ArrayList<>();
            incidenceArrayList.add(this.vertexArray[i]);

            for (int j = 0; j < this.edgeArray.length; j++)
            {
                String[] strings = this.edgeArray[j].split("");//Ребро состоит из 2 элементов, поэтому дальше юзаю 0 и 1
                if(strings[0].equals(this.vertexArray[i])){
                    int nIncidence = 1;
                    if(strings[1].equals(this.vertexArray[i])){
                        nIncidence++;
                    }
                    incidenceArrayList.add("" + nIncidence);
                }
                else{
                    incidenceArrayList.add("" + 0);
                }
            }
            this.arrayAdjacency.add(incidenceArrayList);
        }
    }

    ArrayList<ArrayList<String>> getArrayAdjacency(){
        return this.arrayAdjacency;
    }

    String[] getEdgeArray(){
        return this.edgeArray;
    }

    String[] getVertexArray(){
        return this.vertexArray;
    }

    public String toString(){
        String res = "  ";
        for (int i = 0; i < this.edgeArray.length; i++) {
            res += this.edgeArray[i] + " ";
        }
        res += "\n";

        for (int i = 0; i < this.vertexArray.length; i++) {
            res += this.vertexArray[i];

            for (int j = 0; j < this.edgeArray.length; j++) {
                String[] strings = this.edgeArray[j].split("");//Ребро состоит из 2 элементов, поэтому дальше юзаю 0 и 1
                if(strings[0].equals(this.vertexArray[i])){
                    int nAdjacency = 1;
                    if(strings[1].equals(this.vertexArray[i])){
                        nAdjacency++;
                    }
                    res += "  " + nAdjacency;
                }
                else{
                    res += "  " + 0;
                }
            }

            res += "\n";
        }
        return res;
    }
}

