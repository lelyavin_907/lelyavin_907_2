package ru.kpfu.itis.group907.lelyavin.inf.graphs;

public class ListVertexEdge {
    private String[] edgeArray;
    private String[] vertexArray;

    ListVertexEdge(String[] edgeArray, String[] vertexArray){
        this.edgeArray = new String[edgeArray.length];
        System.arraycopy(edgeArray, 0, this.edgeArray, 0, edgeArray.length);
        this.vertexArray = new String[vertexArray.length];
        System.arraycopy(vertexArray, 0, this.vertexArray, 0, vertexArray.length);
    }

    String[] getEdgeArray(){
        return this.edgeArray;
    }

    String[] getVertexArray(){
        return this.vertexArray;
    }

    public String toString(){
        String res = "Список вершин \n";
        for (int i = 0; i < this.vertexArray.length; i++) {
            res += this.vertexArray[i] + "\n";
        }
        res += "Список ребер\n";
        for (int i = 0; i < this.edgeArray.length; i++) {
            res += this.edgeArray[i] + "\n";
        }
        return res;
    }
}
