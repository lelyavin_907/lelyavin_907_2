package ru.kpfu.itis.group907.lelyavin.inf.graphs;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListAdjacency {
    private String[] edgeArray;
    private String[] vertexArray;
    private ArrayList<ArrayList<String>> arrayVertex;

    ListAdjacency(String[] edgeArray, String[] vertexArray) {
        this.edgeArray = new String[edgeArray.length];
        System.arraycopy(edgeArray, 0, this.edgeArray, 0, edgeArray.length);
        this.vertexArray = new String[vertexArray.length];
        System.arraycopy(vertexArray, 0, this.vertexArray, 0, vertexArray.length);
        this.arrayVertex = new ArrayList<>();
        for (int i = 0; i < this.vertexArray.length; i++) {
            ArrayList<String> listVertex = new ArrayList<>();
            listVertex.add(this.vertexArray[i] + ":");
            for (int j = 0; j < this.edgeArray.length; j++) {
                String[] strings = this.edgeArray[j].split("");
                if (this.vertexArray[i].equals(strings[0])) {
                    listVertex.add(strings[1] + "");
                }
            }
            this.arrayVertex.add(listVertex);
        }
    }

    String[] getEdgeArray(){
        return this.edgeArray;
    }

    String[] getVertexArray(){
        return this.vertexArray;
    }

    public ArrayList<ArrayList<String>> getArrayVertex(){
        return this.arrayVertex;
    }

    public String toString() {
        String res = "";
        for (int i = 0; i < this.vertexArray.length; i++) {
            res += this.vertexArray[i] + ": ";
            for (int j = 0; j < this.edgeArray.length; j++) {
                String[] strings = this.edgeArray[j].split("");
                if (this.vertexArray[i].equals(strings[0])) {
                    res += strings[1] + " ";
                }
            }
            res += "\n";

        }
        return res;
    }
}
