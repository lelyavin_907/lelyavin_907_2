package ru.kpfu.itis.group907.lelyavin.inf.graphs;

public class Main {
    public static void main(String[] args) {
        String[] arrayVertex = {"a", "b", "c", "d"};
        String[] arrayEdge = {"ab", "ba","ab", "dd", "ad"};
        ListAdjacency listAdjacency = new ListAdjacency(arrayEdge, arrayVertex);
//        System.out.println("Лист смежности \n" + listAdjacency);
//        for (int i = 0; i < listAdjacency.getArrayVertex().size(); i++) {
//                System.out.println(listAdjacency.getArrayVertex().get(i));
//        }
        MatrixIncidence matrixIncidence = new MatrixIncidence(arrayEdge,arrayVertex);
//        System.out.println(matrixIncidence);
//        for (int i = 0; i < matrixIncidence.getArrayIncidence().size(); i++) {
//            System.out.println(matrixIncidence.getArrayIncidence().get(i));
//        }

        MatrixAdjacency matrixAdjacency = new MatrixAdjacency(arrayEdge,arrayVertex);
//        for (int i = 0; i < matrixAdjacency.getArrayAdjacency().size(); i++) {
//            System.out.println(matrixAdjacency.getArrayAdjacency().get(i));
//        }
        ListVertexEdge listVertexEdge = new ListVertexEdge(arrayEdge,arrayVertex);
        System.out.println(listVertexEdge);
    }

    public static MatrixAdjacency convertListVEMatrixA(ListVertexEdge listVertexEdge){
        return new MatrixAdjacency(listVertexEdge.getEdgeArray(),listVertexEdge.getVertexArray());
    }

    public static MatrixIncidence convertListVEMatrixI(ListVertexEdge listVertexEdge){
        return new MatrixIncidence(listVertexEdge.getEdgeArray(),listVertexEdge.getVertexArray());
    }

    public static ListAdjacency convertListVEListA(ListVertexEdge listVertexEdge){
        return new ListAdjacency(listVertexEdge.getEdgeArray(),listVertexEdge.getVertexArray());
    }


}
