package ru.kpfu.itis.group907.lelyavin.inf.files;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    private static int i = 0;
    private static ArrayList<Long> fileModifiedArray = new ArrayList<>();
    private static ArrayList<String> nameFileArray  = new ArrayList<>();
    public static void main(String[] args) {
        File repository = new File("src/ru/kpfu/itis/group907/lelyavin");
        nameFile(repository);
        System.out.println(i);
        Collections.sort(fileModifiedArray);
        System.out.println(fileModifiedArray);
        System.out.println(nameFileArray);

    }
    public static void nameFile(File repository){
        for (File packages: repository.listFiles()) {
            if(packages.isFile() && packages.getName().endsWith(".java")){
                i++;
                nameFileArray.add(packages.getName());
                fileModifiedArray.add(packages.lastModified());
            }
            else if(packages.isDirectory()){
                nameFile(packages);
            }
        }

    }
}
