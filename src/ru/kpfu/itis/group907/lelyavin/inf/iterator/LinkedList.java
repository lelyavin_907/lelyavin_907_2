package ru.kpfu.itis.group907.lelyavin.inf.iterator;

public class LinkedList implements Iterable<Node> {
    public Node head;
    public Node tail;

    public LinkedList(Node head) {
        this.head = head;
        this.tail = head;
    }

    public String toString() {
        String string = "";
        Node curr = this.head;
        while (curr != null) {
            string = string + "" + curr;
            curr = curr.next;
        }
        return string;
    }

    public int len() {
        Node curr = this.head;
        int len = 1;
        while (curr != null) {
            curr = curr.next;
            len++;
        }
        return len;
    }

    public Node get(int index) {
        if (index > this.len()) {
            throw new IllegalArgumentException(" Something not is wrong");
        } else if (index == 0) {
            return this.head;
        } else {
            Node curr = this.head;
            for (int i = 1; i <= index; i++) {
                curr = curr.next;
            }
            return curr;
        }
    }


    public void add(Node node) {
        Node curr = this.head;
        while (true) {
            if (curr.next == null) {
                this.tail = node;
                curr.next = node;
                break;
            }
            curr = curr.next;
        }
    }

    public int max() {
        int max = 0;
        Node curr = this.head;
        while (true) {
            if (curr.elem > max) {
                max = curr.elem;
            }
            if (curr.next == null) {
                break;
            }
            curr = curr.next;
        }
        return max;
    }

    public int sum() {
        int sum = 0;
        Node cure = this.head;
        while (cure.next != null) {
            sum += cure.elem;
            cure = cure.next;
        }
        return sum;
    }

    public boolean hasNegative() {
        boolean b = true;
        Node cure = this.head;
        while (cure.next != null) {
            if (cure.elem < 0) {
                b = false;
                break;
            }
            cure = cure.next;
        }
        return b;
    }

    public void removeHead() {
        this.head = this.head.next;
    }

    public void removeTail() {
        Node curr = this.head;
        while (true) {
            if (curr.next.next == null) {
                curr.next = null;
                break;
            }
            curr = curr.next;
        }
    }


    public void removeNode(Node node) {
        Node curr = this.head;
        while (true) {
            if (this.head.next == node.next) {
                this.head = curr.next;
                break;
            }
            if (curr.next.next == node.next) {
                curr.next = curr.next.next;
                break;
            }
            curr = curr.next;
        }
    }


    public void removeА(int index) {
        Node curr = this.head;
        while (true) {
            if (curr.next.next == get(index).next) {
                curr.next = curr.next.next;
                break;
            }
            curr = curr.next;
        }
    }

    public void remove(int k) {
        Node curr = this.head;
        while (true) {
            if (curr.next.next == get(k).next) {
                curr.next = curr.next.next;
                break;
            }
            curr = curr.next;
        }
    }

    public void removeAll(int k) {
        Node curr = this.head;
        while (true) {
            if (curr == null) {
                break;
            }
            if (curr.elem == k) {
                removeNode(curr);
            }
            curr = curr.next;
        }
    }

    public void insert(int m, int k) {
        Node curr = this.head;
        Node twoNode = curr.next;
        int local = k;
        while (curr != null) {
            if (curr.next == null) {
                break;
            }
            if (curr.next.elem == k) {
                if (this.head.elem == local) {
                    twoNode.elem = m;
                    local++;
                }
                curr.elem = m;
                if (curr.next.next == null) {
                    break;
                }
                curr.next.next.elem = m;

            }
            curr = curr.next;
        }
    }

    @Override
    public MyListIterator iterator() {
        return new MyListIterator(this.head, this.tail);
    }
}
