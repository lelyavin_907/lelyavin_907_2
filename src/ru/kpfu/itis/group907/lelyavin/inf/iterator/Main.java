package ru.kpfu.itis.group907.lelyavin.inf.iterator;

public class Main {
    public static void main(String[] args) {
        Node node1 = new Node(1, null);
        Node node2 = new Node(2, node1);
        Node node3 = new Node(3, node2);
        Node node4 = new Node(4, node3);
        Node node5 = new Node(5, node4);

        LinkedList linkedList = new LinkedList(node1);

        linkedList.add(node2);
        linkedList.add(node3);
        linkedList.add(node4);
        linkedList.add(node5);

        for (Node node : linkedList) {
            System.out.println(node);
        }
    }
}
