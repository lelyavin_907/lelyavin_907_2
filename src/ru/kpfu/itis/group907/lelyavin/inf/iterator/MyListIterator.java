package ru.kpfu.itis.group907.lelyavin.inf.iterator;

import java.util.ListIterator;
import java.util.function.Consumer;

public class MyListIterator implements ListIterator<Node> {
    Node head;
    Node tail;
    int indexNext = 0;
    int indexPrevious = 0;

    MyListIterator(Node head, Node tail) {
        this.head = head;
        this.tail = tail;
    }

    @Override
    public boolean hasNext() {
        return this.head != null;
    }

    @Override
    public boolean hasPrevious() {
        return this.tail != null;
    }

    @Override
    public Node previous() {
        Node curr = this.tail;
        Node currIndex = this.tail;
        if (this.tail.previous == null) {
            this.tail = null;
        } else {
            this.tail = this.tail.previous.previous;
        }
        while (currIndex != null) {
            currIndex = currIndex.previous;
            indexPrevious++;
        }
        return curr;
    }

    @Override
    public Node next() {
        Node curr = this.head;
        if (this.head.next == null) {
            this.head = null;
        } else {
            this.head = this.head.next.next;
        }
        indexNext++;
        return curr;
    }


    @Override
    public int nextIndex() {
        return indexNext;
    }

    @Override
    public int previousIndex() {
        return indexPrevious;
    }

    @Override
    public void remove() {
    }

    @Override
    public void forEachRemaining(Consumer<? super Node> action) {
    }

    @Override
    public void set(Node node) {
    }

    @Override
    public void add(Node node) {
    }
}
