package ru.kpfu.itis.group907.lelyavin.inf.iterator;

public class Node {
    public Node next;
    public Node previous;
    public int elem;

    public Node(int elem, Node node) {
        this.elem = elem;
        this.next = null;
        this.previous = node;
    }

    @Override
    public String toString() {
        return this.elem + " ";
    }
}
