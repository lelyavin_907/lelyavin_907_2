package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

//1.Работает

//2.Без изменений

//3.Hi! a b d e


public class Exception6 extends Exception {
    public static void main(String[] args) {
        System.out.println("Hi!");
        try {
            System.out.println("a");
            throw new Exception6();
        } catch (Exception6 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
