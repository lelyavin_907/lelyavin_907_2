package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

import java.util.Scanner;

public class IDNumber {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String string1 = scanner.next();
        String string2 = "";
        String[] strings1;
        String[] strings2;
        int num1 = 0;
        int num2 = 0;
        if (string1.length() == 10) {
            char[] strToArray = string1.toCharArray();
            strings1 = new String[strToArray.length / 2];
            strings2 = new String[strToArray.length / 2];

            int j1 = 0;
            for (int i = 0; i < strToArray.length; i++) {
                if (i < strToArray.length / 2) {
                    strings1[i] = String.valueOf(strToArray[i]);
                } else {
                    strings2[j1] = String.valueOf(strToArray[i]);
                    j1++;
                }
            }
            string1 = "";
            int j2 = 0;
            for (int i = strings1.length - 1; i >= 0; i--) {
                string1 += strings1[i];
            }
            for (int i = strings2.length - 1; i >= 0; i--) {
                string2 += strings2[i];
            }
            try {
                num1 = Integer.parseInt(string1);
                num2 = Integer.parseInt(string2);
                System.out.println("correct");
            } catch (NumberFormatException c) {
                System.out.println("incorrect");
            }
        } else {
            System.out.println("incorrect");
        }
    }
}


