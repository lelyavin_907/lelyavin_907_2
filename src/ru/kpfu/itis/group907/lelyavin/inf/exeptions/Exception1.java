package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

//1.-до изменения
//2-что изменил
//3.что выведет

//1.Exception1 не откого не наследуется => программа просто не запустится

//2.добавил наследование

//3. b d

public class Exception1 extends Exception {
    public static void main(String[] args) {
        try {
            throw new Exception1();
        } catch (Exception1 e) {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }
    }
}
