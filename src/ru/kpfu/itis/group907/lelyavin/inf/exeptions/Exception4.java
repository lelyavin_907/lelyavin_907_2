package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

//1.Программа запустится

//2.Без изменений

//3.a ,потом b ,потом d ,потом e

public class Exception4 {
    public static void main(String[] args) {
        System.out.println("a");
        try {
            System.out.println("b");
        } catch (Exception e) {
            System.out.println("c");
        } finally {
            System.out.println("d");
        }

        System.out.println("e");
    }
}
