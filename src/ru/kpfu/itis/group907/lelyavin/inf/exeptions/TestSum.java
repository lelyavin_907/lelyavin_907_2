package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

public class TestSum {
    public static void test(int x, int y) {
        try {
            if ((x + y) <= 100)
                System.out.println(x + y);
            else {
                throw new SumException();
            }
        } catch (SumException e) {
            System.out.println(e);
        }
    }
}
