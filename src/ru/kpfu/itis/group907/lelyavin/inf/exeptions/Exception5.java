package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

//1.КОд запустится

//2.Убрал finally,т.к. он переопределяет возвращение try,добавил Exception ,если b == 0.

//3.Вывод:11


public class Exception5 {
    public static int divide(int a, int b) {
        try {
            if (b == 0) {
                throw new Exception();
            }
            return a / b;
        } catch (Exception e) {
            return 0;
        }
    }

    public static void main(String[] args) {
        int i = divide(11, 1);
        System.out.println(i);
    }
}
