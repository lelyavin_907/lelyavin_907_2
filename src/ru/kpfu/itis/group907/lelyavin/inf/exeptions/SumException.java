package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

public class SumException extends Exception {
    public String toString() {
        return "The number is greater than 100.";
    }
}
