package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

public class Convert {
    public static String convert(String foot, String inch) {
        int footInt = 0;
        String res = "";
        int inchInt = 0;
        try {
            footInt = Integer.parseInt(foot);
            inchInt = Integer.parseInt(inch);
            if (footInt < 0 || inchInt < 0) throw new Exception();
            res = (footInt * 30.48) + (inchInt * 2.54) + "";
        } catch (java.lang.NumberFormatException e) {
            System.out.println("You must enter integers. Please try again.");
        } catch (Exception e) {
            System.out.println("please enter positive value only ");
        }

        return res;
    }
}
