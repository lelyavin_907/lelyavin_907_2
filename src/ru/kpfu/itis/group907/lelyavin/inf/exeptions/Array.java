package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

public class Array {

    private int[] a;
    private int index;

    public Array() {
        a = new int[100];
        index = 0;
    }

    // adding x to the end of array
    public void add(int x) {
        if (index < a.length) {
            a[index] = x;
            index += 1;
        }
    }

    // adding x on position k. Be sure that position k is applicable to actual size of array.
    public void add(int k, int x) {
        if (k >= 0 && k <= a.length - 1) {
            a[k] = x;
        }
    }

    // clearing the array
    public void clear() {
        while (index < a.length) {
            index++;
            a[index] = 0;
        }
    }

    // check if x is in array
    public boolean contains(int x) {
        boolean b = false;
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x) {
                b = true;
                break;
            }
        }
        return b;
    }

    // remove an element from position k
    public void remove(int k) {
        try {
            a[k] = 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("k не индекс данного массива");
        }
    }

    // remove first occurence of x in array
    public void delete(int x) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x) {
                a[i] = 0;
                break;
            }
        }
    }

    // convert array to string
    public String toString() {
        String string = "";
        for (int i = 0; i < a.length; i++) {
            string += a[i] + " ";
        }
        return string;
    }

}
