package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

public class ThrowsExceptions extends Exception {
    public static void methodThrowsClassCastException() {
        Object x = new Integer(0);
        System.out.println((String) x);
    }

    public static void methodThrowsNullPointerException() {
        Object object = null;
        System.out.println(object.equals(object));
    }
}
