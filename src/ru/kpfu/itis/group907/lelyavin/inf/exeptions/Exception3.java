package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

//1.Программа запустится

//2.Без изменений

//3.Выведется a и b

public class Exception3 {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println("a");
            throw new NullPointerException();
        } catch (NullPointerException e) {
            System.out.println("b");
            throw new Exception();
        } catch (Exception e) {
            System.out.println("c");
        }
    }
}
