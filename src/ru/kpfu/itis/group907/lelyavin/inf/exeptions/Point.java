package ru.kpfu.itis.group907.lelyavin.inf.exeptions;

public class Point {
    int x;
    int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int slope(int num3, int num4) {
        int res = 0;
        try {
            res = Math.abs((num4 - this.y) / (num3 - this.x));
        } catch (ArithmeticException e) {
            System.out.println("Zero is an invalid denominator. Please try again");
        }
        return res;
    }
}
