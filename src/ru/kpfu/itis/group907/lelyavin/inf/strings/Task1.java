package ru.kpfu.itis.group907.lelyavin.inf.strings;

import java.util.ArrayList;
import java.util.Collections;

public class Task1 {
    public static void main(String[] args) {
        MyString myString1 = new MyString("AcB");
        MyString myString2 = new MyString("ABc");
        MyString myString3 = new MyString("ABC");
        ArrayList<MyString> myStringArrayList = new ArrayList<>();
        myStringArrayList.add(myString1);
        myStringArrayList.add(myString2);
        myStringArrayList.add(myString3);
        Collections.sort(myStringArrayList);
        System.out.println(myStringArrayList);
    }
}
