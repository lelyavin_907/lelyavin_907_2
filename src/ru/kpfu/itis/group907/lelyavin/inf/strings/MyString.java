package ru.kpfu.itis.group907.lelyavin.inf.strings;


import java.util.Collections;

//Нельзя не создавать поле т.к. String - final
public class MyString implements Comparable<MyString> {
    private String word;

    MyString(String word){
        this.word = word;
    }

    @Override
    public String toString() {
        return "MyString{" +
                "word='" + word + '\'' +
                '}';
    }

    @Override
    public int compareTo(MyString o) {
        int nWord = this.word.length();
        int nO = o.word.length();
        int min = Math.min(nWord,nO);

        for (int i = 0; i < min; i++) {
            char c1 = this.word.charAt(i);
            char c2 = o.word.charAt(i);

            if (c1 != c2) {
                c1 = Character.toUpperCase(c1);
                c2 = Character.toUpperCase(c2);
                if (c1 != c2) {
                    c1 = Character.toLowerCase(c1);
                    c2 = Character.toLowerCase(c2);
                    if (c1 != c2) {
                        return c1 - c2;
                    }
                }
            }
        }
        return nWord - nO;
    }
}
