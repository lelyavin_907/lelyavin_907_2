package ru.kpfu.itis.group907.lelyavin.inf.strings;

public class Task4 {
    public static void main(String[] args) {
        String string = "Hello my name is Nikita and you";
        System.out.println(joinOdd(string));
    }

    public static String joinOdd(String string){
        String[] strings = string.split(" ");
        string = "";
        for (int i = 0; i < strings.length; i += 2) {
            string += strings[i] + " ";
        }
        return string;
    }
}
