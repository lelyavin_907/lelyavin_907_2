package ru.kpfu.itis.group907.lelyavin.inf.strings;

import java.util.HashMap;

public class Task2 {
    public static void main(String[] args) {
            String string = "Hello my name is Nikita and you";
        System.out.println(count(string));
    }

    public static HashMap<Character, Integer> count(String string){
        HashMap<Character, Integer> characterHashMap = new HashMap<>();
        String[] strings = string.split(" ");
        String stringLocal = "";
        for (int i = 0; i < strings.length; i++) {
            stringLocal += strings[i];
        }
        for (int i = 0; i < stringLocal.length(); i++) {
            Character letter = Character.toLowerCase(stringLocal.charAt(i));
            if(characterHashMap.containsKey(letter)){
                int x = characterHashMap.get(letter) + 1;
                characterHashMap.put(letter, x);
            }
            else{
                characterHashMap.put(letter, 1);
            }
        }
        return characterHashMap;
    }
}
