package ru.kpfu.itis.group907.lelyavin.sem4variant;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.*;

public class PolinomTest {

    @Test
    public void insert() throws FileNotFoundException {
        Polinom polinom = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        polinom.insert(5,7);

        String monoms = "5-7";
        Monom monom = new Monom(monoms);
        Assert.assertEquals(monom.toString(),polinom.linkedList.get(polinom.linkedList.getSize() - 1).toString());
    }

    @Test
    public void combine() throws FileNotFoundException {
        Polinom polinom = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        String s = "5*x^3 + 12*x^2 + 6*x^1";
        polinom.combine();
        Assert.assertEquals(s,polinom.toString());
    }

    @Test
    public void delete() throws FileNotFoundException {
        Polinom polinom = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        polinom.delete(3);
        String s = "4*x^2 + 4*x^2 + 2*x^1 + 4*x^2";
        Assert.assertEquals(s,polinom.toString());
    }

    @Test
    public void sum() throws FileNotFoundException {
        Polinom polinom = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        Polinom polinomTest = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        String s = "8*x^3 + 8*x^2";
        polinom.sum(polinomTest);
        Assert.assertEquals(s,polinom.toString());
    }

    @Test
    public void derivate() throws FileNotFoundException {
        Polinom polinom = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        String s = "8*x^1 + 12*x^2 + 4*x^0";
        polinom.derivate();
        Assert.assertEquals(s,polinom.toString());
    }

    @Test
    public void value() throws FileNotFoundException {
        Polinom polinom = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        int x = 120;
        polinom.value(2);
        Assert.assertEquals(x,polinom.value(2));
    }

    @Test
    public void deleteOdd() throws FileNotFoundException {
        Polinom polinom = new Polinom("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\sem4variant\\filename.txt");
        String s = "4*x^2";
        polinom.deleteOdd();
        Assert.assertEquals(s,polinom.toString());
    }
}