package ru.kpfu.itis.group907.lelyavin.sem4variant;

public class DoublyLinkedList<E>
{
    private Node<E> header;
    private Node<E> trailer;
    private int size;

    public DoublyLinkedList() {
        this.header = new Node<E>(null, null, null);
        this.trailer = new Node<E>(header, null, null);
        this.header.next = trailer;
        this.size = 0;
    }

    public int getSize(){
        return size;
    }

    private Node<E> access(int i) {
        Node<E> current = header;
        for (int j = -1; j < i; j++)
            current = current.next;

        return current;
    }

    public boolean isEmpty() {
        return (this.size == 0);
    }

    public void add(int i, E e) {
        if (i < 0 || i > this.size)
            throw new IndexOutOfBoundsException();

        Node<E> elem = this.access(i - 1);
        elem.next.prev = new Node<E>(elem, elem.next, e);
        elem.next = elem.next.prev;

        this.size++;
    }

    public void addFirst(E e) {
        this.add(0, e);
    }

    public void addLast(E e) {
        trailer.prev.next = new Node<E>(trailer.prev, trailer, e);
        trailer.prev = trailer.prev.next;

        this.size++;
    }

    public void delete(int i) {
        if (i < 0 || i >= this.size)
            throw new IndexOutOfBoundsException();

        Node<E> elem = this.access(i - 1);
        elem.next.next.prev = elem;
        elem.next = elem.next.next;

        this.size--;
    }

    public void delete(E e) {
        Node<E> current = header.next;

        while (current != trailer) {
            if (current.val.equals(e)) {
                current.prev.next = current.next;
                current.next.prev = current.prev;
                this.size--;
            }
            current = current.next;
        }
    }

    public void deleteFirst() {
        this.delete(0);
    }

    public void deleteLast() {
        trailer.prev.prev.next = trailer;
        trailer.prev = trailer.prev.prev;

        this.size--;
    }

    public void set(int i, E e) {
        this.access(i).val = e;
    }

    public E get(int i) {
        return this.access(i).val;
    }

    public void print() {
        Node<E> current = header.next;
        for (int j = 0; j < this.size; j++) {
            System.out.print(current.val.toString() + " ");
            current = current.next;
        }
        System.out.println();
    }
}


class Node<E>
{
    Node<E> prev;
    Node<E> next;
    E val;

    Node(Node<E> prev, Node<E> next, E val) {
        this.prev = prev;
        this.next = next;
        this.val = val;
    }
}