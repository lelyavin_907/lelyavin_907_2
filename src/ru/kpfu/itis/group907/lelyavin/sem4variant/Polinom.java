package ru.kpfu.itis.group907.lelyavin.sem4variant;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;


public class Polinom {
    public DoublyLinkedList<Monom> linkedList;

    Polinom(String fileName) throws FileNotFoundException {
        this.linkedList = new DoublyLinkedList<>();
        FileReader fileReader = new FileReader(fileName);
        Scanner scanner = new Scanner(fileReader);
        while (scanner.hasNextLine()) {
            String string = scanner.nextLine();
            Monom monom = new Monom(string);
            this.linkedList.addLast(monom);
        }
    }

    public String toString() {
        String string = "";
        for (int i = 0; i < linkedList.getSize(); i++) {
            if (i != linkedList.getSize() - 1) {
                string += linkedList.get(i) + " + ";
            } else {
                string += linkedList.get(i);
            }
        }
        return string;
    }

    public void insert(int coef, int deg) {
        Monom monom = new Monom();
        monom.setDegree(deg);
        monom.setCoeff(coef);

        this.linkedList.addLast(monom);
    }

    public void combine() {
        ArrayList<Monom> monomArrayList = new ArrayList<>();
        for (int i = 0; i < this.linkedList.getSize(); i++) {
            monomArrayList.add(this.linkedList.get(i));
        }

        MyComparator myComparator = new MyComparator();
        Collections.sort(monomArrayList, myComparator);

        int size = monomArrayList.size();
        for (int i = size - 1; i >= 0; i--) {
            int j = i - 1;
            int localCoeff = monomArrayList.get(i).getCoeff();
            int localDegree = monomArrayList.get(i).getDegree();
            this.linkedList.delete(i);

            if (i != 0) {
                int i1 = i;
                while (monomArrayList.get(i1).getDegree() == monomArrayList.get(j).getDegree()) {
                    localCoeff += monomArrayList.get(j).getCoeff();

                    this.linkedList.delete(j);
                    i--;
                    j--;
                    if (i <= 0) {
                        break;
                    }
                }
            }

            Monom monom = new Monom();
            monom.setCoeff(localCoeff);
            monom.setDegree(localDegree);
            this.linkedList.addLast(monom);
        }
    }

    public void delete(int deg) {
        for (int i = this.linkedList.getSize() - 1; i >= 0; i--) {
            if (this.linkedList.get(i).getDegree() == deg) {
                this.linkedList.delete(i);
            }
        }
    }

    public void sum(Polinom polinom) {
        for (int i = 0; i < polinom.linkedList.getSize(); i++) {
            this.linkedList.addLast(polinom.linkedList.get(i));
        }
        this.combine();
    }

    public void derivate() {
        for (int i = 0; i < this.linkedList.getSize(); i++) {
            if (this.linkedList.get(i).getCoeff() != 0) {
                this.linkedList.get(i).derivate();
            }
            else{
                this.linkedList.delete(i);
            }
        }
    }

    public int value(int x){//5*x^2  x = 2
        int bn = 0;
        this.combine();
        bn = this.linkedList.get(0).getCoeff();
        int j = 1;
        for (int i = this.linkedList.get(0).getDegree() - 1; i >= 0; i--) {
            if(j > this.linkedList.getSize() - 1){
                bn = bn * x;
            }
            else if( this.linkedList.get(j).getDegree() == 0){
                bn = this.linkedList.get(j).getCoeff() + bn * x;
            }
            else if(this.linkedList.get(j).getDegree() != i){
                bn = bn * x;
            }

            else{
                bn = this.linkedList.get(j).getCoeff() + bn * x;
                j++;
            }
        }
        return bn;
    }

    public void deleteOdd(){
        for (int i = 0; i < this.linkedList.getSize(); i++) {
            if(this.linkedList.get(i).getCoeff() % 2 != 0){
                this.linkedList.delete(i);
                i--;
            }
        }
    }
}

class MyComparator implements Comparator<Monom> {

    @Override
    public int compare(Monom o1, Monom o2) {
        if (o1.getDegree() == o2.getDegree()) {
            return 0;
        }
        return o1.getDegree() > o2.getDegree() ? 1 : -1;
    }
}