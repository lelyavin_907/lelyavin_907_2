package ru.kpfu.itis.group907.lelyavin.sem4variant;

public class Monom {
    private int coeff;
    private int degree;

    Monom(){
        this.coeff = 0;
        this.degree = 0;
    }

    public int getCoeff(){
        return this.coeff;
    }

    public int getDegree(){
        return this.degree;
    }

    public void setCoeff(int coeff){
        this.coeff = coeff;
    }

    public void setDegree(int degree){
        this.degree = degree;
    }

    Monom(String koefDegree){
        String[] local = koefDegree.split("-");
        this.coeff =  Integer.parseInt(local[0]);
        this.degree =  Integer.parseInt(local[1]);
    }

    public void derivate() {
        this.coeff = this.coeff * this.degree;
        this.degree--;
    }

    public String toString(){
        return this.coeff + "*x^" + this.degree;
    }
}
