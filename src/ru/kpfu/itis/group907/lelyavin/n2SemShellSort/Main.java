package ru.kpfu.itis.group907.lelyavin.n2SemShellSort;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main{
    public static int a;
    public static void sort(int[] array) {
        int inner, outer;
        int temp;
        int h = 1;
        while (h <= array.length / 3) {
            h = h * 3 + 1;
            a++;
        }
        while (h > 0) {
            for (outer = h; outer < array.length; outer++) {
                temp = array[outer];
                inner = outer;
                while (inner > h - 1 && array[inner - h] >= temp) {
                    array[inner] = array[inner - h];
                    inner -= h;
                    a++;
                }
                array[inner] = temp;
                a++;
            }
            h = (h - 1) / 3;
            a++;
        }
    }
    public static void main(String[] args) throws IOException {
        createData();
        FileReader fileReader = new FileReader("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\n2SemShellSort\\data.txt");
        Scanner scanner = new Scanner(fileReader);
        String[] strings = scanner.nextLine().split(" ");
        int[] array = new int[strings.length];

        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(strings[i]);
        }

        long startArraySorting = System.nanoTime();

        sort(array);

        long finishArraySorting = System.nanoTime();
        long timeConsumedMillisArraySorting = finishArraySorting - startArraySorting;

        System.out.println("Итог: " + Arrays.toString(array));
        System.out.print(array.length + " ");
        System.out.print(a + " ");
        System.out.print(timeConsumedMillisArraySorting);
    }

    public static void createData() throws IOException {
        FileWriter fileWriter = new FileWriter("C:\\Users\\Никита\\Desktop\\1.Инфа\\lelyavin_907_2\\src\\ru\\kpfu\\itis\\group907\\lelyavin\\n2SemShellSort\\data.txt");
        int[] array;
        array = new int[(int)(Math.random() * 10000)];

        for (int i = 0; i < array.length; i++) {
            array[i] = ((int)(Math.random() * 1000));
            fileWriter.write(array[i] + " ");
        }

        fileWriter.close();
    }
}
