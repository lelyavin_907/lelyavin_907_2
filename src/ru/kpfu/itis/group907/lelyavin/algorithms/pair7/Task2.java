package ru.kpfu.itis.group907.lelyavin.algorithms.pair7;

public class Task2 {
    public static void main(String[] args) {
        int[] arrayNum = {100, -1, -10, -10, -2, -5, -2, 100};
        int maxSum = 0;
        int localSum = 0;

        for (int i = 0; i < arrayNum.length; i++) {
            localSum += arrayNum[i];
            maxSum = Math.max(maxSum, localSum);

            if(localSum < 0){
                localSum = 0;
            }
        }

        System.out.println(maxSum);
    }
}
