package ru.kpfu.itis.group907.lelyavin.algorithms.pair7;

import java.util.Collections;
import java.util.Scanner;

public class Task1 {
    public static void main() {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int res = 0;
        if(x / 5000 > 0){
            res = x / 5000;
            x = x % 5000;
        }
        if(x / 1000 > 0){
            res += x / 1000;
            x = x % 1000;
        }
        if(x / 500 > 0){
            res += x / 500;
            x = x % 500;
        }
        if(x / 100 > 0){
            res += x / 100;
            x = x % 100;
        }
        if(x / 50 > 0){
            res += x / 50;
        }
        System.out.println(res);
    }
}
