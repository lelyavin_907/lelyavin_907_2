package ru.kpfu.itis.group907.lelyavin.algorithms.pair9;

public class Task3 {
    public static void main(String[] args) {
        String string = "1211111111111 333";
        string = string.trim();
        String[] stringArray = string.split(" ");
        try {
            if (Long.parseLong(stringArray[0]) > (long) Integer.MAX_VALUE) {
                System.out.println(Integer.MAX_VALUE);
            } else if (Long.parseLong(stringArray[0]) < (long) Integer.MIN_VALUE) {
                System.out.println(Integer.MIN_VALUE);
            } else {
                System.out.println(Integer.parseInt(stringArray[0]));
            }
        }
        catch (NumberFormatException e){//Не совсм ясно как отличить ошибку парса от ошибки слишком большого числа
                                        // , если и то и то NumberFormatException
            System.out.println(0);
        }
    }
}
