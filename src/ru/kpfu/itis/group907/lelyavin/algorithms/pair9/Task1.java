package ru.kpfu.itis.group907.lelyavin.algorithms.pair9;

import java.util.ArrayList;
import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        String string1 = "abcd";
        String string2 = "abcd";
        String saveString = string1;
        String[] arrayString1 = string1.split("");

        ArrayList<String> string1ArrayList = new ArrayList<>(Arrays.asList(arrayString1));


        for (int i = 0; i < arrayString1.length; i++) {

            if(saveString.equals(string2)){
                System.out.println("Строки одинаковые со сдвигом влево");
                break;
            }

            String localString = string1ArrayList.get(0);
            string1ArrayList.remove(0);
            string1ArrayList.add(localString);

            saveString = "";
            for (int j = 0; j < string1ArrayList.size(); j++) {
                saveString += string1ArrayList.get(j);
            }
        }
    }
}
