package ru.kpfu.itis.group907.lelyavin.algorithms.contest2.Task1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Stock {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int nRequests = scanner.nextInt();
        int deep = scanner.nextInt();
        ArrayList<Requests> requestsArrayListBuy = new ArrayList<>();
        ArrayList<Requests> requestsArrayListSell = new ArrayList<>();


        for (int i = 0; i < nRequests; i++) {
            String buyOrSell = scanner.next();
            int cost = scanner.nextInt();
            int countRequest = scanner.nextInt();
            Requests requests = new Requests(buyOrSell, cost,countRequest);
            if(buyOrSell.equals("B")) {
                requestsArrayListBuy.add(requests);
            }
            else{
                requestsArrayListSell.add(requests);
            }
        }

        MyComparator myComparator = new MyComparator();



        for (int i = requestsArrayListBuy.size() - 1; i > 0 ; i--) {
            for (int j = requestsArrayListBuy.size() - 1; j >= 0; j--) {
                if(requestsArrayListBuy.get(i).cost == requestsArrayListBuy.get(j).cost && i != j){
                    requestsArrayListBuy.get(i).countRequest += requestsArrayListBuy.get(j).countRequest;
                    requestsArrayListBuy.get(j).cost = -5;
                }
            }
        }
        for (int i = requestsArrayListSell.size() - 1; i >= 0 ; i--) {
            for (int j = requestsArrayListSell.size() - 1; j >= 0; j--) {
                if(requestsArrayListSell.get(i).cost == requestsArrayListSell.get(j).cost && i != j){
                    requestsArrayListSell.get(i).countRequest += requestsArrayListSell.get(j).countRequest;
                    requestsArrayListSell.get(j).cost = -5;
                }
            }
        }
        requestsArrayListBuy.sort(myComparator);
        requestsArrayListSell.sort(myComparator);

        ArrayList<Requests> arrayListSell = new ArrayList<>();

        for (int i = 0; i < requestsArrayListSell.size(); i++) {
            if(requestsArrayListSell.get(i).cost == -5){
                continue;
            }
            arrayListSell.add(requestsArrayListSell.get(i));
        }


        ArrayList<Requests> arrayListBuy = new ArrayList<>();

        for (int i = 0; i < requestsArrayListBuy.size(); i++) {
            if(requestsArrayListBuy.get(i).cost == -5){
                continue;
            }
            arrayListBuy.add(requestsArrayListBuy.get(i));
        }



        if(arrayListSell.size() >= deep) {
            for (int i = 0; i < deep; i++) {
                System.out.print(arrayListSell.get(i).buyOrSell);
                System.out.print(" ");
                System.out.print(arrayListSell.get(i).cost);
                System.out.print(" ");
                System.out.print(arrayListSell.get(i).countRequest);
                System.out.println();
            }
        }
        else {
            for (int i = 0; i < arrayListSell.size(); i++) {
                System.out.print(arrayListSell.get(i).buyOrSell);
                System.out.print(" ");
                System.out.print(arrayListSell.get(i).cost);
                System.out.print(" ");
                System.out.print(arrayListSell.get(i).countRequest);
                System.out.println();
            }
        }


        if(arrayListBuy.size() >= deep) {
            for (int i = 0; i < deep; i++) {
                System.out.print(arrayListBuy.get(i).buyOrSell);
                System.out.print(" ");
                System.out.print(arrayListBuy.get(i).cost);
                System.out.print(" ");
                System.out.print(arrayListBuy.get(i).countRequest);
                System.out.println();
            }
        }
        else {
            for (int i = 0; i < arrayListBuy.size(); i++) {
                System.out.print(arrayListBuy.get(i).buyOrSell);
                System.out.print(" ");
                System.out.print(arrayListBuy.get(i).cost);
                System.out.print(" ");
                System.out.print(arrayListBuy.get(i).countRequest);
                System.out.println();
            }
        }
    }




    public static class Requests{
        private String buyOrSell;
        private int cost;
        private int countRequest;

        Requests(String buyOrSell, int cost, int countRequest){
            this.buyOrSell = buyOrSell;
            this.cost = cost;
            this.countRequest = countRequest;
        }

        public String toString(){
            return this.buyOrSell + " " + this.cost + " " + this.countRequest;
        }

    }


    public static class MyComparator implements Comparator<Requests>{

        @Override
        public int compare(Requests requests1, Requests requests2) {
            if(requests1.cost == requests2.cost){
                return 0;
            }
            return requests1.cost > requests2.cost ? -1 : 1;
        }
    }
}
