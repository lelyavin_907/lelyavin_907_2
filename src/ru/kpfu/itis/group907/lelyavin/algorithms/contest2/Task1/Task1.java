package ru.kpfu.itis.group907.lelyavin.algorithms.contest2.Task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nRequests = scanner.nextInt();
        int depth = scanner.nextInt();
        ArrayList<Requests> requestsArrayList = new ArrayList<>();
        for (int i = 0; i < nRequests; i++) {
            requestsArrayList.add(new Requests(scanner.next(), scanner.nextInt(), scanner.nextInt()));
        }


        MyComparator myComparator = new MyComparator();
        Collections.sort(requestsArrayList, myComparator);

        for (int i = 0; i < requestsArrayList.size() - 1; i++) {
            for (int j = i + 1; j < requestsArrayList.size(); j++) {
                if (requestsArrayList.get(i).cost == requestsArrayList.get(j).cost) {
                    requestsArrayList.get(i).countRequest += requestsArrayList.get(j).countRequest;
                    requestsArrayList.remove(j);
                    j--;
                }
            }
        }

        int localSell = 0;
        int localBuy = 0;
        for (int i = 0; i < requestsArrayList.size(); i++) {

            if(requestsArrayList.get(i).buyOrSell.equals("S") && localSell < depth){
                localSell++;
                System.out.println(requestsArrayList.get(i));
            }

            else if(requestsArrayList.get(i).buyOrSell.equals("B") && localBuy < depth){
                localBuy++;
                System.out.println(requestsArrayList.get(i));
            }
        }
    }


    public static class Requests{
        private String buyOrSell;
        private int cost;
        private int countRequest;

        Requests(String buyOrSell, int cost, int countRequest){
            this.buyOrSell = buyOrSell;
            this.cost = cost;
            this.countRequest = countRequest;
        }

        public String toString(){
            return this.buyOrSell + " " + this.cost + " " + this.countRequest;
        }

    }

    public static class MyComparator implements Comparator<Requests> {

        @Override
        public int compare(Requests requests1, Requests requests2) {
            if(requests1.cost == requests2.cost){
                return 0;
            }
            return requests1.cost > requests2.cost ? -1 : 1;
        }
    }
}
