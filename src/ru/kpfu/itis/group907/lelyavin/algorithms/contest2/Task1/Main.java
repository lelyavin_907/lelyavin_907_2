package ru.kpfu.itis.group907.lelyavin.algorithms.contest2.Task1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nRequests = scanner.nextInt();
        int depth = scanner.nextInt();
        ArrayList<Requests> requestsArrayList = new ArrayList<>();
        for (int i = 0; i < nRequests; i++) {
            requestsArrayList.add(new Requests(scanner.next(), scanner.nextInt(), scanner.nextInt()));
        }


        MyComparator myComparator = new MyComparator();
        requestsArrayList.sort(myComparator);

        int currCost = 0;
        ArrayList<Requests> agrRequests = new ArrayList<>();
        for (Requests req : requestsArrayList)
            if (req.cost == currCost) {
                Requests last = agrRequests.get(agrRequests.size()-1);
                last.countRequest += req.countRequest;
            } else {
                agrRequests.add(req);
                currCost = req.cost;
            }

        int localSell = 0;
        int localBuy = 0;
        for (int i = 0; i < agrRequests.size(); i++) {

            if(agrRequests.get(i).buyOrSell.equals("S") && localSell < depth){
                localSell++;
                System.out.println(agrRequests.get(i));
            }

            else if(agrRequests.get(i).buyOrSell.equals("B") && localBuy < depth){
                localBuy++;
                System.out.println(agrRequests.get(i));
            }
        }
    }

    public static class Requests{
        private String buyOrSell;
        private int cost;
        private int countRequest;

        Requests(String buyOrSell, int cost, int countRequest){
            this.buyOrSell = buyOrSell;
            this.cost = cost;
            this.countRequest = countRequest;
        }

        public String toString(){
            return this.buyOrSell + " " + this.cost + " " + this.countRequest;
        }

    }

    public static class MyComparator implements Comparator<Requests> {

        @Override
        public int compare(Requests requests1, Requests requests2) {
            if(requests1.cost == requests2.cost){
                return 0;
            }
            return requests1.cost > requests2.cost ? -1 : 1;
        }
    }
}
