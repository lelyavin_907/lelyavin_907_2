package ru.kpfu.itis.group907.lelyavin.algorithms.pair3.Task1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество значений в массиве");
        int[] ints = new int[scanner.nextInt()];
        for (int i = 0; i < ints.length; i++) {
            System.out.println("Введите значения массива");
            ints[i] = scanner.nextInt();
        }
        int i = 0;
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        while (i < ints.length) {
            int local = ints[i];
            if (hashMap.containsKey(local)) {
                int x = hashMap.get(local) + 1;
                hashMap.put(local, x);
            } else {
                hashMap.put(local, 1);
            }
            i++;
        }
        for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            hashMap.get(hashMap.values());
            System.out.println(entry + " раз");
        }
    }
}
