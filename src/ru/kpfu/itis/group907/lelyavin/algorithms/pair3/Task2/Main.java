package ru.kpfu.itis.group907.lelyavin.algorithms.pair3.Task2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество значений в массиве");
        int n = scanner.nextInt();
        int[] ints = new int[n];
        for (int i = 0; i < ints.length; i++) {
            System.out.println("Введите значения массива");
            ints[i] = scanner.nextInt();
        }

        HashMap<Integer, Integer> hashMap = new HashMap();
        for (int i = 0; i < ints.length; i++) {
            if (hashMap.containsKey(ints[i])) {
                hashMap.put(ints[i], hashMap.get(ints[i]) + 1);
            } else {
                hashMap.put(ints[i], 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            if (entry.getValue() > Math.floor(n / 2.0)) {
                System.out.println(entry.getKey());
            }
        }
    }
}
