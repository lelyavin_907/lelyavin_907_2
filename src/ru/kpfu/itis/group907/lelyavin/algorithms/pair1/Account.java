package ru.kpfu.itis.group907.lelyavin.algorithms.pair1;

public class Account<T1, T2 extends Number> implements Accountable<T1, T2> {
    private T1 id;
    private T2 sum;

    Account(T1 id, T2 sum) {
        this.id = id;
        this.sum = sum;
    }

    @Override
    public T1 getId() {
        return this.id;
    }

    @Override
    public T2 getSum() {
        return this.sum;
    }

    public void setSum(T2 sum) {
        this.sum = sum;
    }

    public void addToSum(T2 sum) {
        this.sum = (T2) (Number) (this.sum.longValue() + sum.longValue());
    }

    public void subtractFromSum(T2 sum) {
        this.sum = (T2) (Number) (this.sum.longValue() - sum.longValue());
    }
}
