package ru.kpfu.itis.group907.lelyavin.algorithms.pair1;

public interface Accountable<T1, T2 extends Number> {
    T1 getId();

    T2 getSum();
}
