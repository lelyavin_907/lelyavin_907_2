package ru.kpfu.itis.group907.lelyavin.algorithms.pair1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество значений в связанном списке ");
        int countNode = in.nextInt() - 1;

        System.out.println("Введите значение головного элемента");
        int valueHeadNode = in.nextInt();
        LinkedList linkedList = new LinkedList(new Node(valueHeadNode, null));

        for (int i = 1; i <= countNode; i++) {
            System.out.println("Введите значение " + i + " элемента ");
            Node node = new Node(in.nextInt(), null);
            linkedList.add(node);
        }
        Node curr1 = linkedList.head;
        Node curr2 = curr1;
        for (int i = 0; i <= countNode; i++) {
            curr1 = curr1.next;
            curr2 = curr1;
        }

//        Node node =  linkedList.get(1);
//        linkedList.remove(3);
//        System.out.println(linkedList);
        linkedList.insert(4, 5);
//        System.out.println(linkedList);


    }
}
