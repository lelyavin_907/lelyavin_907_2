package ru.kpfu.itis.group907.lelyavin.algorithms.pair1;

public class Node {
    public Node next;
    public int elem;

    public Node(int elem, Node node) {
        this.elem = elem;
        this.next = node;
    }

    @Override
    public String toString() {
        return this.elem + " ";
    }
}
