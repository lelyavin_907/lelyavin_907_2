package ru.kpfu.itis.group907.lelyavin.algorithms.сontest3;

import java.util.ArrayList;
import java.util.Scanner;

public class Task3_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String stringUser = scanner.nextLine();

        int m = Integer.parseInt(scanner.nextLine());

        int[] a = new int[stringUser.length()];

        for (int i = 1; i < stringUser.length(); i++) {
            a[i] = a[i-1] + (stringUser.charAt(i - 1) == (stringUser.charAt(i)) ? 1 : 0);
        }

        for (int i = 0; i < m; i++) {
//            String[] s = scanner.nextLine().split(" ");
            int x1 = scanner.nextInt();
            int x2 = scanner.nextInt();
            System.out.println(a[x2 - 1] - a[x1 - 1]);
        }
    }
}