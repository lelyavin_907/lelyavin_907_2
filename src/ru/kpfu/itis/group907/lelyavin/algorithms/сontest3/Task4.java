package ru.kpfu.itis.group907.lelyavin.algorithms.сontest3;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String numUserK = scanner.nextLine();
        String saveN = scanner.nextLine();
        long res = 0;
        ArrayList<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < saveN.length(); i++) {
            int x = Character.getNumericValue((saveN.charAt(i)));
            res += x;
            arrayList.add(x);
        }

        Collections.sort(arrayList);


        if(res < Long.parseLong(numUserK)){
            int j = 0;
            long saveNum = Long.parseLong(numUserK) -  res;
            while (saveNum > 0){
                saveNum -= 9 - arrayList.get(j);
                j++;
            }
            System.out.println(j);
        }
        else{
            System.out.println(0);
        }
    }
}
