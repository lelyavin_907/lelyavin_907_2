package ru.kpfu.itis.group907.lelyavin.algorithms.сontest3;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nYear = scanner.nextInt();
        int localResult = 0;
        int finalResult = 0;

        for (int i = 0; i < nYear; i++) {

            int numUser = scanner.nextInt();

            if(numUser < 0 && numUser % 2 == 0){
                localResult++;
            }

            else if(numUser % 2 == -1 || numUser > 0 ){
                localResult = 0;
            }

            if(localResult > finalResult){
                finalResult = localResult;
            }
        }
        System.out.println(finalResult);
    }
}
