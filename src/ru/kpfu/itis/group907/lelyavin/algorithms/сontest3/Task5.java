package ru.kpfu.itis.group907.lelyavin.algorithms.сontest3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int nArray = scanner.nextInt();

        //Сортировка Листа с числами юзера
        ArrayList<Integer> listNum = new ArrayList<>();
        for (int i = 0; i < nArray; i++) {
            listNum.add(scanner.nextInt());
        }
        Collections.sort(listNum);

        int countRes = 0;

        for (int i = 0; i < listNum.size(); i++) {
            if(listNum.get(i) > nArray){
                countRes++;
            }
            else if(i >= 1) {
                if (listNum.get(i).equals(listNum.get(i - 1))) {
                    countRes++;
                }
            }
        }

        System.out.println(countRes);
    }
}
