package ru.kpfu.itis.group907.lelyavin.algorithms.pair2;

public class List {
    public static int[] numeral(int a) {
        String s = String.valueOf(a);
        int[] ints = new int[s.length()];
        int i = s.length();
        while (a != 0) {
            ints[i - 1] = a % 10;
            i--;
            a /= 10;
        }
        return ints;
    }

    public static int compare(int a, int b) {
        int[] a2 = List.numeral(a);
        int[] b2 = List.numeral(b);
        int[] a1;
        int[] b1;
        int save = 0;
        if (a2.length > b2.length) {
            a1 = b2;
            b1 = a2;
            save = a;
            a = b;
            b = save;
        } else {
            a1 = a2;
            b1 = b2;
        }
        for (int i = 0; i < a1.length; i++) {
            if (a1[i] > b1[i]) {
                return a;
            }
            if (a1[i] < b1[i]) {
                return b;
            }
            for (int j = 0; j < b1.length; j++) {
                if (a1[i] > b1[j]) {
                    return a;
                }
                if (a1[i] < b1[j]) {
                    return b;
                }
            }
        }
        return a;
    }
}
