package ru.kpfu.itis.group907.lelyavin.algorithms.pair2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainForList {
    public static void main(String[] args) {
        int[] numbers = {5, 5, 4, 1, 5, 15, 556, 666, 56};
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++) {
            arrayList.add(numbers[i]);
        }
        MyComparator myComparator = new MyComparator();
        Collections.sort(arrayList, myComparator);
        System.out.println(arrayList);
    }
}

class MyComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        String string1 = o1 + "" + o2;
        String string2 = o2 + "" + o1;
        System.out.println(string1 + " " + string2);
        if (Integer.parseInt(string1) == Integer.parseInt(string2)) {
            return 0;
        }
        return Integer.parseInt(string1) > Integer.parseInt(string2) ? -1 : 1;

    }
}
