package ru.kpfu.itis.group907.lelyavin.algorithms.pair2;

import java.util.Scanner;

public class MainForMerge {
    public static int len(int[] ints) {
        int length = 0;
        for (int i = 0; i < ints.length; i++) {
            for (int j = 0; j < ints[i]; j++) {
                length++;
            }
        }
        return length;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Количество массивов");
        int k = scanner.nextInt();
        int[] m = new int[k];
        for (int i = 0; i < k; i++) {
            System.out.println("Размер массива " + i);
            m[i] = scanner.nextInt();
        }
        int iForMergeArray = 0;
        int[] intsFirst = new int[m[0]];
        int[] mergeArray = new int[m[0] + m[1]];
        for (int i = 0; i < intsFirst.length; i++) {
            System.out.println("Значения " + 0 + "массива");
            intsFirst[i] = scanner.nextInt();
        }
        int[] intsDouble = new int[m[1]];
        for (int j = 0; j < m[1]; j++) {
            System.out.println("Значения " + 1 + "массива");
            intsDouble[j] = scanner.nextInt();
        }
        int x = 0;
        int y = 0;
        while ((x < intsFirst.length) && (y < intsDouble.length)) {
            if (intsFirst[x] <= intsDouble[y]) {
                mergeArray[iForMergeArray] = intsFirst[x];
                x++;
            } else {
                mergeArray[iForMergeArray] = intsDouble[y];
                y++;
            }
            iForMergeArray++;
        }
        while (x < intsFirst.length) {
            mergeArray[iForMergeArray] = intsFirst[x];
            x++;
            iForMergeArray++;
        }
        while (y < intsDouble.length) {
            mergeArray[iForMergeArray] = intsDouble[y];
            y++;
            iForMergeArray++;
        }
        if (k > 2) {
            for (int i = 2; i < k; i++) {
                int[] localArray = new int[m[i]];
                for (int j = 0; j < m[i]; j++) {
                    System.out.println("Введите знаение " + i + "");
                    localArray[j] = scanner.nextInt();
                }
                int[] merge = new int[mergeArray.length + localArray.length];
                for (int j = 0; j < mergeArray.length; j++) {
                    merge[j] = mergeArray[j];
                }
                int x1 = 0;
                int y1 = 0;
                iForMergeArray = 0;
                while ((x1 < mergeArray.length) && (y1 < localArray.length)) {
                    if (mergeArray[x1] <= localArray[y1]) {
                        merge[iForMergeArray] = mergeArray[x1];
                        x1++;
                    } else {
                        merge[iForMergeArray] = localArray[y1];
                        y1++;
                    }
                    iForMergeArray++;
                }
                while (x1 < mergeArray.length) {
                    merge[iForMergeArray] = mergeArray[x1];
                    x1++;
                    iForMergeArray++;
                }
                while (y1 < localArray.length) {
                    merge[iForMergeArray] = localArray[y1];
                    y1++;
                    iForMergeArray++;
                }
                mergeArray = merge;
            }
        }
        for (int i = 0; i < mergeArray.length; i++) {
            System.out.println(mergeArray[i]);
        }

    }
}
