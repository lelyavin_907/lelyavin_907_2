package ru.kpfu.itis.group907.lelyavin.algorithms.pair8.task3;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 7, 7, 8};
        System.out.println(search(array));
    }

    public static ArrayList<Integer> search(int[] array) {
        int leftSide = 0;
        int rightSide = array.length - 1;
        int mid;
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length / 2; i++) {
            mid = (leftSide + rightSide) / 2;
            if (array[mid] != array[mid + 1] && array[mid] != array[mid - 1]) {
                arrayList.add(mid);
                break;
//            } else if () {
//                rightSide = mid + 1;
//            } else {
//                result = mid;
//                break;
            }
        }
        return arrayList;
    }
}
