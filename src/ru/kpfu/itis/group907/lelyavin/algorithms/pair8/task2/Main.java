package ru.kpfu.itis.group907.lelyavin.algorithms.pair8.task2;

public class Main {
    public static void main(String[] args) {
        int[] x = {16, 15, 23, 24, 25};
        System.out.println(search(x));
    }

    public static int search(int[] x) {
        int result = 0;
        int mid;
        int leftSide = 0;
        int rightSide = x.length;

        for (int i = 0; i < x.length / 2; i++) {
            mid = (leftSide + rightSide) / 2;
            if (x[mid] < x[mid - 1] && x[mid] > x[mid + 1]) {
                leftSide = mid + 1;
            } else if (x[mid] > x[mid - 1] && x[mid] < x[mid + 1]) {
                rightSide = mid + 1;
            } else {
                result = mid;
                break;
            }
        }
        return result;
    }

}
