//package ru.kpfu.itis.group907.lelyavin.algorithms.contest.Task7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashMap<String,Integer> friendsMap = new HashMap<>();

        int nFriends = scanner.nextInt();
        String[] friendsList = new String[nFriends];
        ArrayList<String> friendsArray = new ArrayList<>();
        for (int i = 0; i < nFriends; i++) {
            String friend = scanner.next();
            friendsList[i] = (friend);
        }

        for (int i = nFriends - 1; i >= 0; i--) {
            if (friendsMap.containsKey(friendsList[i])) {
            } else {
                friendsMap.put(friendsList[i], 1);
                friendsArray.add(friendsList[i]);
            }
        }
        for (int i = 0; i < friendsArray.size(); i++) {
            System.out.println(friendsArray.get(i));
        }
    }
}
