package ru.kpfu.itis.group907.lelyavin.algorithms.contest.Task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        int sizeArrayList1 = scanner.nextInt();
        for (int i = 0; i < sizeArrayList1; i++) {
            arrayList1.add(scanner.nextInt());
        }
        Collections.sort(arrayList1);

        ArrayList<Integer> arrayList2 = new ArrayList<>();
        int sizeArrayList2 = scanner.nextInt();
        for (int i = 0; i < sizeArrayList2; i++) {
            arrayList2.add(scanner.nextInt());
        }

        for (int i = 0; i < sizeArrayList2; i++) {
            System.out.println((search(arrayList1, arrayList2.get(i))));
        }
    }

    public static int search(ArrayList<Integer> arrayList1, Integer arrayList2Element) {
        int leftSide = 0;
        int rightSide = arrayList1.size() - 1;
        int mid = (leftSide + rightSide) / 2;
        while (leftSide < rightSide) {
            if (arrayList1.get(mid) > arrayList2Element)
                rightSide = mid - 1;
            else
                leftSide = mid + 1;
            mid = (leftSide + rightSide) / 2;
        }
        if (arrayList2Element < arrayList1.get(mid))
            return mid;
        else
            return mid + 1;
    }
}

