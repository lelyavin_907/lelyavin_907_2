package ru.kpfu.itis.group907.lelyavin.algorithms.contest.Task3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        System.out.println("Количество фонарей");
        int numberLamps = scanner.nextInt();
//        System.out.println("length of street");
        int length = scanner.nextInt();


        ArrayList<Integer> lamps = new ArrayList<>();
        for (int i = 0; i < numberLamps; i++) {
            lamps.add(scanner.nextInt());
        }

        Collections.sort(lamps);

        double distance = 2 * Math.max(lamps.get(0), length - lamps.get(numberLamps - 1));
        for (int i = 0; i < numberLamps - 1; i++) {
            if (lamps.get(i + 1) - lamps.get(i) > distance) {
                distance = (lamps.get(i + 1) - lamps.get(i));
            }
        }
        System.out.println(distance / 2.0);
    }
}
