package ru.kpfu.itis.group907.lelyavin.algorithms.contest.Task6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nWordsPolandBall = scanner.nextInt();
        int nWordsEnemiesBall = scanner.nextInt();

        //Обьявление,заполнение,сортировка массивов слов
        ArrayList<String> wordsPolandBall = new ArrayList<>();
        ArrayList<String> wordsEnemiesBall = new ArrayList<>();
//        System.out.println(nWordsPolandBall);
        for (int i = 0; i < nWordsPolandBall; i++) {
            wordsPolandBall.add(scanner.next());
        }
//        System.out.println(wordsEnemiesBall);
        for (int i = 0; i < nWordsEnemiesBall; i++) {
            wordsEnemiesBall.add(scanner.next());
        }
        Collections.sort(wordsPolandBall);
        Collections.sort(wordsEnemiesBall);
//        System.out.println(Comparison(wordsPolandBall,wordsEnemiesBall));

    }
    public static String Comparison(ArrayList<String> wordsPolandBall ,ArrayList<String> wordsEnemiesBall){


        if(wordsPolandBall.size() > wordsEnemiesBall.size()){
            return "YES";
        }
        else if(wordsEnemiesBall.size() > wordsPolandBall.size()){
            return "NO";
        }

        String res = "";
        int i = wordsPolandBall.size() - 1;
        int i1 = wordsEnemiesBall.size() - 1;
        while (wordsPolandBall.size() > 0 || wordsEnemiesBall.size() > 0){
            int local = 0;
            for (int j = wordsEnemiesBall.size() - 1; j >= 0; j--) {
                if (wordsPolandBall.get(i).equals(wordsEnemiesBall.get(j))) {
                    wordsPolandBall.remove(i);
                    wordsEnemiesBall.remove(j);
                    i--;
                    i1--;
                    local++;
                    if ((wordsEnemiesBall.size() <= 0)) {
                        return "YES";
                    }
                    break;
                }
            }

            if(local == 0){
                wordsPolandBall.remove(i);
                i--;
                if((wordsEnemiesBall.size() <= 0)){
                    return "YES";
                }
            }
            int local2 = 0;
            for (int j = wordsPolandBall.size() - 1; j >= 0; j--) {
                if(wordsEnemiesBall.get(i1).equals(wordsPolandBall.get(j))){
                    wordsPolandBall.remove(j);
                    wordsEnemiesBall.remove(i1);
                    i--;
                    i1--;
                    local2++;
                    if((wordsPolandBall.size() <= 0)){
                        return "NO";
                    }
                    break;
                }
            }
            if(local2 == 0){
                wordsEnemiesBall.remove(i1);
                i1--;
                if((wordsPolandBall.size() <= 0)){
                    return "NO";
                }
            }
        }
        if (wordsPolandBall.size() > wordsEnemiesBall.size()){return "YES";
        }
        else{
            return "NO";
        }
    }
}
