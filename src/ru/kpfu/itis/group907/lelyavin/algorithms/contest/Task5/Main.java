package ru.kpfu.itis.group907.lelyavin.algorithms.contest.Task5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberCities = scanner.nextInt();
        int populationTomsk = scanner.nextInt();

        ArrayList<Point> points = new ArrayList<>();

        for (int i = 0; i < numberCities; i++) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            int population = scanner.nextInt();
            double sqrt =  Math.sqrt(x * x + y * y);
            Point point = new Point(sqrt,population);
            points.add(point);
        }
        Collections.sort(points);

        int checkPopulation = 1000000;
        double resRadius = 0;
        for (int i = 0; i < numberCities; i++) {
            populationTomsk += points.get(i).population;
            if(populationTomsk >= checkPopulation){
                resRadius = points.get(i).radius;
                break;
            }
        }
        if(populationTomsk < checkPopulation){
            System.out.println(-1);
        }
        else{
            System.out.println(resRadius);
        }
    }
    public static class Point implements Comparable<Point> {
        double radius;
        int population;

        Point(double radius, int population){
            this.radius = radius;
            this.population = population;
        }

        @Override
        public int compareTo(Point o) {
            if(this.radius == o.radius){
                return 0;
            }
            return this.radius > o.radius ? 1:-1;
        }
    }
}
