package ru.kpfu.itis.group907.lelyavin.algorithms.pair4;

import java.math.BigInteger;
import java.util.Arrays;

/*public class Main {
    public static void main(String[] args) {
        int[] ints = {1,2,3};
        int[][] ints1 = new int[factorial(ints.length)][ints.length];
        for (int i = 0; i < factorial(ints.length)/ints.length; i++) {
            ints1[i] = ints.clone();
            for (int j = 0; j < ints.length; j++) {
                if(i != j) {
                    int local = ints1[i][j];
                    ints1[i][i] = ints1[i][j];
                    ints1[i][j] = local;
                }
                for (int k = 0; k < ints.length; k++) {
                    System.out.print(" " + ints1[i][k]);
                }
                System.out.println();
            }
        }
    }
*/
public class Main {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3};
        int size = factorial(arr.length);
        int max = arr.length - 1;
        int shift = max;
        System.out.println("max: " + max + " size: " + size + " shift: " + shift);
        while (size > 0) {  
            int local = arr[shift];
            arr[shift] = arr[shift - 1];
            arr[shift - 1] = local;
            print(arr);
            size--;
            if (shift < 2) {
                shift = max;
            } else {
                shift--;
            }
        }
    }

    static void print(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }


    public static int factorial(int n) {
        int mem = n;
        int res = 1;
        if (n == 0) {
            return 0;
        }
        for (int i = 0; i < mem - 1; i++) {
            res *= n;
            n -= 1;
        }
        return res;
    }
}
