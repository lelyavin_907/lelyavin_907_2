package ru.kpfu.itis.group907.lelyavin.algorithms.pair4.Task3;

import java.util.ArrayList;

public class Task3 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);

        subsets(arrayList, new ArrayList<Integer>(), 0);
    }

    public static void subsets(ArrayList<Integer> arrayList, ArrayList<Integer> subset, int x){
        System.out.println(subset);
        for (int i = x; i < arrayList.size(); i++) {
            subset.add(arrayList.get(i));
            subsets(arrayList, subset, i + 1);
            subset.remove(subset.size() - 1);
        }
    }
}
