package ru.kpfu.itis.group907.lelyavin.algorithms.pair4.Task2;

public class Task2 {
    public static void main(String[] args) {
        int[] array = {1,2,3,4};
        permute(array, 0 , array.length - 1);
    }

    public static void permute(int[] array, int start, int end){
        if(start == end){
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }
            System.out.println();
        }
        else{
            for (int i = start; i <= end; i++) {
                array = swap(array, start, i);
                permute(array, start + 1 , end);
                array = swap(array, start , i);
            }
        }
    }

    public static int[] swap(int[] array, int i, int j){
        int t = array[i];
        array[i] = array[j];
        array[j] = t;
        return array;
    }
}
