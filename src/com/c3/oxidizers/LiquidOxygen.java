package com.c3.oxidizers;

import com.c3.exceptions.InsufficientMatterException;

public class LiquidOxygen implements Oxidizer {
    private int OxygenAtomsCount = 2;
    private int mass = 0;

    public LiquidOxygen(int x){
        this.mass = x;
    }


    @Override
    public int getNitrogenAtomsCount() {
        return 0;
    }

    @Override
    public int getOxygenAtomsCount() {
        return OxygenAtomsCount;
    }

    @Override
    public int getMass() {
        return mass;
    }

    @Override
    public void decreaseMass(int mass) throws InsufficientMatterException {
        if (this.mass - mass < 0)throw new InsufficientMatterException(this.mass - mass);
        else this.mass -= mass;
    }
}
