package com.c3.exceptions;

public class InsufficientMatterException extends Exception {
    private int left;
    public InsufficientMatterException(int x){
       this.left = x;
    }

    public int getLeft() {
        return left;
    }

    public String toString() {
        return "У вас получилось отрицательное значние" + left;
    }
}
