package com.c3.tank;

import com.c3.common.IMatter;
import com.c3.exceptions.InsufficientMatterException;

public class Tank<T extends IMatter> {
    public T fuel;

    public Tank(T fuel){
        this.fuel = fuel;
    }

    public T next(int mass) throws InsufficientMatterException {
        fuel.decreaseMass(mass);
        return fuel;
    }

    public T remainingMass(){
        return fuel;
    }
}
