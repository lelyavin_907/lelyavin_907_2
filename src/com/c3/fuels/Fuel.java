package com.c3.fuels;

import com.c3.common.IMatter;

public interface Fuel extends IMatter {

    public int getCarbonAtomsCount();

    public int getHydrogenAtomsCount();

    public int getNitrogenAtomsCount();

    public int getOxygenAtomsCount();
}
