package com.c3.fuels;

import com.c3.exceptions.InsufficientMatterException;

public class Hydrogen implements Fuel {
    private int OxygenAtomsCount = 2;
    private int mass = 0;

    public Hydrogen(int x){
        this.mass = x;
    }

    @Override
    public int getCarbonAtomsCount() {
        return 0;
    }

    @Override
    public int getHydrogenAtomsCount() {
        return 2;
    }

    @Override
    public int getNitrogenAtomsCount() {
        return 0;
    }

    @Override
    public int getOxygenAtomsCount() {
        return 0;
    }

    @Override
    public int getMass() {
        return this.mass;
    }

    @Override
    public void decreaseMass(int mass) throws InsufficientMatterException {
        if (this.mass - mass < 0)throw new InsufficientMatterException(this.mass - mass);
        else this.mass -= mass;
    }
}
