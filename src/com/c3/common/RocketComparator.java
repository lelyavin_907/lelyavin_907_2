package com.c3.common;

import com.c3.exceptions.UnstableEngineException;

import java.util.Comparator;

public class RocketComparator implements Comparator<Rocket> {

    @Override
    public int compare(Rocket o1, Rocket o2) {
        int res = 0;
        int o1Burn = 0;
        int o2Burn = 0;
        try {
            o1Burn += o1.burn();
            o2Burn += o2.burn();
        } catch (UnstableEngineException e) {
            System.out.println("следующий");
        }
        return o1Burn - o2Burn;
    }
}
